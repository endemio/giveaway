Vue.config.errorHandler = function (err, vm, info)  {
    console.log('[Global Error Handler]: Error in ' + info + ': ' + err);
};

var vt = new Vue({
    name: "admin",

    el: "#admin",

    delimiters: ['${', '}'],

    data: () =>({
        giveaways:[],

        email:'',

        /* pagination */
        pages:[], /* pages for navigations */
        disabledbutton:[], /* any disabled buttons */
        listPage:1, /* current page */
        sumPages:1, /* summ pages */
        pageItemClass: 'page-item', /* button class */
        activeClass: 'active', /* css active */


    }),
    mounted(){

    },
    created() {
        this.listGiveaway(1);
    },
    methods: {
        listGiveaway: function (page) {
            let data = new FormData();
            data.append('page',page);
            axios.post('/api/admin/giveaways/list',data)
                .then(response => {
                    // JSON responses are automatically parsed.

                    console.log(response.data);

                    if (typeof response.data.list !== "undefined"){

                        let list = response.data.list;

                        for (let i=0;i<list.length;i++) {
                            this.giveaways.push({
                                'code':list[i].code,
                                'path':list[i].path,
                                'id':list[i].id,
                                'comments':list[i].comments,
                                'likes':list[i].likes,
                                'vaucher':list[i].vaucher,
                                'user_id':list[i].user_id,
                                'unique':list[i].unique,
                            });
                        }

                        this.sumPages = parseInt((this.giveaways.length)/20)+1;
                        this.pagination(1);
                    }

                }).catch(e => {
                    getErrorAxios(e);
            })
        },
        editGiveaway: function () {


        },
        saveGiveaway: function () {
            let data = new FormData();
            axios.post('/api/admin/giveaway/save',data)
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data);

                }).catch(e => {
                    getErrorAxios(e);
            })

        },
        removeGiveaway: function (id,index) {
            if (confirm('Do you really want remove giveaway "'+id +'"?')) {
                let data = new FormData();
                data.append('id',id);
                axios.post('/api/admin/giveaway/remove', data)
                    .then(response => {
                        // JSON responses are automatically parsed.
                        console.log(response.data);
                        if (response.data.error == null) {
                            this.giveaways.splice(index, 1);
                        }
                    }).catch(e => {
                    getErrorAxios(e);
                })
            }
        },

        editPayment: function () {
            let data = new FormData();
            axios.post('/api/admin/giveaways/list',data)
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data);

                }).catch(e => {
                    getErrorAxios(e);
            })

        },
        removePayment: function () {
            let data = new FormData();
            axios.post('/api/admin/giveaways/list',data)
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data);

                }).catch(e => {
                    getErrorAxios(e);
            })

        },

        pagination: function (page) {

            this.listPage = page;
            this.pages = [];
            this.pages.push(1);
            for (let i = this.listPage-5; i< this.listPage+5;i++){
                if (i > 1 && i<this.sumPages) {
                    this.pages.push(i);
                }
            }
            this.pages.push(this.sumPages);
        },
        timestamp(unixtime) {
            return moment(unixtime*1000).format('MM/DD/YY HH:mm:ss');
        },

    },
    computed:{

    }
});

function getErrorAxios(error) {
    // Error
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
    } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
    }
}