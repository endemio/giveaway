Vue.config.errorHandler = function (err, vm, info)  {
    console.log('[Global Error Handler]: Error in ' + info + ': ' + err);
};

var vt = new Vue({
    name: "main",

    el: "#main",

    delimiters: ['${', '}'],

    data: () =>({
        csrf_token:'',
        premium: false,
        voucher: '243423r23',
        giveawayPath:'',
        postExist: false,
        submit:false,

        /* instagram data */
        code:'BtwlsNFg7du',
        likes:0,
        comments:0,
        image:'',
        profile_img:'',
        username:'',
        location:'',
        caption:'',
        profile_url:'',

        /* show elements */
        isShowPremiumInput:false,

        /* options */
        duplicates: false,
        allcomments: false,
        repost: false,
        follow: '',
        phrase: ''

    }),
    mounted(){
        if (typeof this.$refs.csrf_token !== 'undefined') {
            this.csrf_token = this.$refs.csrf_token.dataset.value;
        }
        if (typeof this.$refs.csrf_token_giveaway !== 'undefined') {
            this.csrf_token = this.$refs.csrf_token_giveaway.dataset.value;
        }
    },
    created() {
        this.getPostData();
    },
    methods: {
        startGiveaway: function () {

            this.submit = true;

            const data = new FormData();
            data.append('code',this.code);
            data.append('token',this.csrf_token);
            data.append('voucher',this.voucher);
            data.append('options',JSON.stringify({'duplicates':this.duplicates,'allcomments':this.allcomments,'repost':this.repost,'follow':this.follow,'phrase':this.phrase}));

            axios.post('/api/start-giveaway',data)
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data);

                    if (response.data.status === true){
                        this.giveawayPath =response.data.path;
                    }

                    this.submit = false;
                }).catch(e => {
                getErrorAxios(e);
            })
        },
        getPostData: function () {
            if (this.code.length === 11) {
                const data = new FormData();
                data.append('code', this.code);
                axios.post('/api/get-post-data', data)
                    .then(response => {
                        // JSON responses are automatically parsed.
                        console.log(response.data);
                        if (response.data.status === true){
                            console.log('Status true');
                            this.postExist  = true;
                            this.likes      = response.data.post.likes;
                            this.comments   = response.data.post.comments;
                            this.image      = response.data.post.image;
                            this.username   = response.data.post.username;
                            this.location   = response.data.post.location;
                            this.profile_img= response.data.post.profile_pic;
                            this.caption    = response.data.post.caption;
                            this.profile_url= 'https://www.instagram.com/'+this.username;
                        }
                    }).catch(e => {
                        getErrorAxios(e);
                })
            }
        },
        checkvoucher: function (code) {
            console.log('vaucjer '+code);
            if (this.voucher.length === 10) {
                const data = new FormData();
                data.append('voucher', this.voucher);
                axios.post('/api/check-voucher', data)
                    .then(response => {
                        // JSON responses are automatically parsed.
                        console.log(response.data);
                        if (response.data.status === 1){
                            this.premium = true;
                        } else if (response.data.status === 0){
                            this.premium = false;
                        } else if (response.data.status === false){
                            this.premium = false;
                        } else if (response.data.status >1){
                            this.premium = false;
                        }

                        console.log(this.premium);
                    }).catch(e => {
                    getErrorAxios(e);
                })
            }
        },
        addOptionPremiumIcon: function(){
            if (this.premium === ''){

            }
        },
        showVoucherInput: function () {
            this.isShowPremiumInput = !this.isShowPremiumInput;
        },
        /*validEmail: function (email) {
            let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },*/
    },
    computed:{

    },
    watch: {
        voucher: function (value) {
            this.checkvoucher(value);
        }
    }
});

function getErrorAxios(error) {
    // Error
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
    } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
    }
}