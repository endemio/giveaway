Vue.component('modal', {
    template: "#modal-template",
    props: ["show"],
    data: function() {
        return {
            comment: "",
            card: {
                cvc: '',
                number: '',
                expiry: ''
            },

            //elements
            cardNumber: '',
            cardExpiry: '',
            cardCvc: '',
            stripe: null,
            showModal: false,

            // errors
            stripeError: '',
            cardCvcError: '',
            cardExpiryError: '',
            cardNumberError: '',

            loading: false,
        };
    },
    methods: {
        close: function() {
            this.$emit("close");
            this.comment = '';
        },
        postComment: function() {
            // some other logic
            this.close();
        },
        setUpStripe() {
            if (window.Stripe === undefined) {
                alert('Stripe V3 library not loaded!');
            } else {
                const stripe = window.Stripe('pk_test_Qh5byreWMVf2YTudG2Ield6F');
                this.stripe = stripe;

                const elements = stripe.elements();
                this.cardCvc = elements.create('cardCvc');
                this.cardExpiry = elements.create('cardExpiry');
                this.cardNumber = elements.create('cardNumber');

                this.cardCvc.mount('#card-cvc');
                this.cardExpiry.mount('#card-expiry');
                this.cardNumber.mount('#card-number');

                this.listenForErrors();
            }
        },
        listenForErrors() {
            const vm = this;

            this.cardNumber.addEventListener('change', (event) => {
                vm.toggleError(event);
                vm.cardNumberError = '';
                vm.card.number = event.complete ? true : false
            });

            this.cardExpiry.addEventListener('change', (event) => {
                vm.toggleError(event);
                vm.cardExpiryError = '';
                vm.card.expiry = event.complete ? true : false
            });

            this.cardCvc.addEventListener('change', (event) => {
                vm.toggleError(event);
                vm.cardCvcError = '';
                vm.card.cvc = event.complete ? true : false
            });
        },

        toggleError (event) {
            if (event.error) {
                this.stripeError = event.error.message;
            } else {
                this.stripeError = '';
            }
        },

        submitFormToCreateToken() {
            this.clearCardErrors();
            let valid = true;

            if (!this.card.number) {
                valid = false;
                this.cardNumberError = "Card Number is Required";
            }
            if (!this.card.cvc) {
                valid = false;
                this.cardCvcError = "CVC is Required";
            }
            if (!this.card.expiry) {
                valid = false;
                this.cardExpiryError = "Month is Required";
            }
            if (this.stripeError) {
                valid = false;
            }
            if (valid) {
                this.createToken()
            }
        },

        createToken() {
            this.stripe.createToken(this.cardNumber).then((result) => {
                if (result.error) {
                    this.stripeError = result.error.message;
                } else {
                    const token = result.token.id;
                    console.log(token);
                    //send the token to your server
                    //clear the inputs

                    let data = new FormData();
                    data.append('token',token);
                    data.append('email','mail@endemic.ru');
                    axios.post('/api/stripe/payment',data)
                        .then(response => {
                            // JSON responses are automatically parsed.
                            console.log(response.data);
                        }).catch(e => {
                        getErrorAxios(e);
                    })

                }
            })
        },

        clearElementsInputs() {
            this.cardCvc.clear()
            this.cardExpiry.clear()
            this.cardNumber.clear()
        },

        clearCardErrors() {
            this.stripeError = ''
            this.cardCvcError = ''
            this.cardExpiryError = ''
            this.cardNumberError = ''
        },

        reset() {
            this.clearElementsInputs()
            this.clearCardErrors()
        }
    },
    mounted: function() {
        document.addEventListener("keydown", e => {
            if (this.show && e.keyCode == 27) {
                this.close();
            }
        });
        this.setUpStripe();
    },
    computed: {
        cardType: function() {
            return Stripe.card.cardType(this.cardNumber);
        },
        cardTypeClass: function() {
            switch (this.cardType) {
                case 'Visa': return 'is-visa';
                case 'MasterCard': return 'is-mastercard';
                case 'American Express': return 'is-amex';
                case 'Discover': return 'is-discover';
            }
        },
    }
})

const app = new Vue({
    el: '#billing',
    delimiters: ['${', '}'],
    data: {
        showModal: false,
        card: {
            cvc: '',
            number: '',
            expiry: ''
        },

        //elements
        cardNumber: '',
        cardExpiry: '',
        cardCvc: '',
        stripe: null,
        showModal: false,

        // errors
        stripeError: '',
        cardCvcError: '',
        cardExpiryError: '',
        cardNumberError: '',

        loading: false,
    },

    mounted() {

    },
    methods: {

    }
})

function getErrorAxios(error) {
    // Error
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
    } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
    }
}