Vue.config.errorHandler = function (err, vm, info)  {
    console.log('[Global Error Handler]: Error in ' + info + ': ' + err);
};

var vt = new Vue({
    name: "giveaway",

    el: "#giveaway",

    delimiters: ['${', '}'],

    data: () =>({

    }),
    mounted(){

    },
    created() {
        this.getGiveawayStatus();
    },
    methods: {
        /* Get giveaway status */
        getGiveawayStatus: function () {
            axios.get('/api/check-giveaway-data/'+getCode())
                .then(response => {
                    // JSON responses are automatically parsed.
                    console.log(response.data);

                }).catch(e => {
                getErrorAxios(e);
            })
        }
    },
    computed:{

    }
});

function getErrorAxios(error) {
    // Error
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
    } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
    }
}

function getCode() {
    return document.head.querySelector("[name~=code][content]").content;
}