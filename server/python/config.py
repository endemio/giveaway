import os

AMQP_HOST = "crane-01.rmq.cloudamqp.com"
AMQP_PORT = 5672
AMQP_VHOST = "nsomqyve"
AMQP_USER = "nsomqyve"
AMQP_PASSWORD = "aHs2Nhf-Mg3r0WAx_6y0dw7qaHg7DM8o"
AMQP_EXCHANGE  = 'check-exchange'

AMQP_QUEUE_TASK = "instagram"
AMQP_QUEUE_RESULT = "application"

DATA_ACCOUNT_PATH = os.path.join(str(os.getcwd()), "/home/endemic/work/php/giveaway/data/python/accounts")
DATA_POST_PATH = os.path.join(str(os.getcwd()), "/home/endemic/work/php/giveaway/data/python/posts")

DATA_COMMENTS_LIMIT = 10000
DATA_LIKES_LIMIT = 10000
DATA_FOLLOWERS_LIMIT = 10000
DATA_FOLLOWINGS_LIMIT = 10000