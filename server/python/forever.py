#!/usr/bin/python
# -*- coding: utf-8 -*-

from subprocess import Popen
import time
from random import randrange
import sys

filename = sys.argv[1]
while True:
    print("\nStarting " + filename)
    p = Popen("python36 " + filename + " > ./scraper-" + str(randrange(100000)) + ".log 2>&1" , shell=True)
    time.sleep(300)
    p.wait()