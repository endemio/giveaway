#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
from InstagramAPI import InstagramAPI
import time
import pika
import json
import imageio
import requests
import logging
import re
import traceback
import config
from pathlib import Path
from ig_accounts import ig_config

imageio.plugins.ffmpeg.download()

com_limit = config.DATA_COMMENTS_LIMIT
like_limit = config.DATA_LIKES_LIMIT
fol_limit = config.DATA_FOLLOWERS_LIMIT
foling_limit = config.DATA_FOLLOWINGS_LIMIT


def send(ch,method,answer):
    try:
        answer = "{" + answer + "}"
        ch.basic_ack(delivery_tag = method.delivery_tag)
        channel.basic_publish(exchange=config.AMQP_EXCHANGE,
                              routing_key=config.AMQP_QUEUE_RESULT,
                              body=answer)
    except pika.exceptions.ConnectionClosed:
        logging.error (' ConnectionClosed.')


def callback(ch, method, properties, body):
    body = (body.decode('ascii'))
    logging.info("Body: {}".format(body))
    body = json.loads(body)

    if str(body['task']) == 'getlist':  # Get list for account

        logging.info ('Task: '+body['task'] +':'+body['type'] +':'+ body['account'])

        action  = str(body['type'])
        start   = int(body['start'])
        target  = str(body['account'])
        token   = str(body['token'])
        limit   = int(body['limit'])
        filename = "{}-{}.data".format(target, body['type'])
        try:
            if action == "followers":  # Собираем подписчиков, start приходит из body
                logging.info ('Scraping followers start')
                result = []
                result = getTotalFollowers(api, target, start, limit)
                with open(os.path.join(str(path_acc), filename), 'a', encoding='utf-8') as f:
                    for i in range(len(result)):
                        f.write(str(result[i]) + "\n")

            if action == "following": # Собираем подписки, start приходит из body
                logging.info ('Scraping following start')
                result = []
                result = getTotalFollowings(api, target, start, limit)
                logging.info ('Followings number =' + str(len(result)))
                with open(os.path.join(str(path_acc), filename), 'a', encoding='utf-8') as f:
                    for i in range(len(result)):
                        f.write(str(result[i]) + "\n")

            if action == "following_exists": # Собираем подписки, start приходит из body
                action = "following"

            logging.info ('Scraping complete')

            answer = '"task":"listresult","token":"{}","account":"{}","type":"{}","result":"complete","error":"none"'.format( \
                token, \
                target, \
                action)
        except Exception as e:
            answer = '"task":"listresult","token":"{}","account":"{}","type":"{}","result":"complete","error":"{}"'.format(
                token, \
                target, \
                action, \
                e)
            logging.error(traceback.format_exc())
        send(ch,method,answer)

    elif str(body['task']) == 'checkaction':  # Check actions to post

        logging.info ('Task: '+body['task'] +':'+body['type'] +':'+ body['account'] +':'+ body['shortcode'])

        add_part = "/?__a=1"
        url = "https://www.instagram.com/p/"
        try:
            target = str(body['account'])
        except:
            target = ""
        try:
            post_link = str(body['shortcode'])
        except:
            pass
        try:
            token = str(body['token'])
        except:
            pass
        filename = "{}-{}.data".format(post_link, body['type'])
        try:
            post = requests.get(url + post_link + add_part).json()
            cur_path = os.path.join(str(path_post), filename)
            post_id = str(post['graphql']['shortcode_media']['id'])
            post_owner = str(post['graphql']['shortcode_media']['owner']['username'])
        except:
            logging.error("Invalid URL")
            answer = '"task":"checkresult","token":"{}","account":"{}","post":"{}","type":"{}","text":"{}","code":"{}","result":false'.format(
                token, \
                target, \
                post_link, \
                body['type'], \
                'Invalid URL', \
                404 \
                )
            send(ch,method,answer)
            return
        try:
            caption = str(
                post['graphql']['shortcode_media']['edge_media_to_caption']['edges'][0]['node']['text'])
        except:
            caption = ''
        try:
            text = body['text']
        except:
            pass
        try:
            N = 0
            if str(body['type']) == "likes":  # Get likes to shortcode
                result = getArrayFromFilename(cur_path)
                if target in result:
                    answer = answerString(token,target,post_link,body['type'],'200','','true','')
                else:
                    api.getMediaLikers(post_id)
                    jz = api.LastJson
                    for y in range(len(jz['users'])):
                        if jz['users'][y]['username'] not in result :
                            with open(cur_path, 'a', encoding='utf-8') as f:
                                f.write(str(jz['users'][y]['username']) + "\n")
                    if target in result:
                        answer = answerString(token,target,post_link,body['type'],'200','','true',jz['user_count'])
                    else:
                        answer = answerString(token,target,post_link,body['type'],'200','','false',jz['user_count'])
                send(ch,method,answer)
                return
        except:
            answer = '"task":"checkresult","token":"{}","post":"{}","type":"{}","result":false'.format(
                token, \
                post_link, \
                body['type'] \
                )
            logging.error(traceback.format_exc())
        send(ch,method,answer)
    else:
        send(ch,method,body)


#-------------------------------- Subprograms ----------------------------

def answerString (token,account,post_link,type,code,text,result,counter):
    answer = '"task":"checkresult","token":"{}","account":"{}","post":"{}","type":"{}","code":"{}","text":"{}","counter":"{}","result":{}'.format(
        token, \
        account, \
        post_link, \
        type, \
        code, \
        text, \
        counter, \
        result
        )
    return answer

def getArrayFromFilename (filename):
    file = Path(filename)
    if file.exists():
        with open(filename, 'r') as f:
            result = f.read().splitlines()
    else:
        result = []
    return result

def getTotalFollowers(api, target, start, limit):
    next_max_id = True
    api.searchUsername(target)
    jz = api.LastJson
    user_id = jz['user']['pk']
    follower_count = jz['user']['follower_count']
    time.sleep(1)
    followers = []
    if (limit > 0):
        cnt = limit
    else:
        cnt = follower_count
    while next_max_id:
        if next_max_id is True:
            next_max_id = ''
        _ = api.getUserFollowers(user_id, maxid=next_max_id)
        js = api.LastJson
        for i in range(len(js['users'])):
            try:
                nickname = js['users'][i]['username']
                followers.append(nickname)
                if (cnt <= len(followers)):
                    return followers
            except:
                pass
        time.sleep(1.75)
        next_max_id = api.LastJson.get('next_max_id', '')
    return followers

def getTotalFollowings(api, target, N, limit):
    next_max_id = True
    api.searchUsername(target)
    jz = api.LastJson
    user_id = jz['user']['pk']
    following_count = jz['user']['following_count']
    followings = []
    if (limit > 0):
        cnt = limit
    else:
        cnt = following_count

    while next_max_id:
        if next_max_id is True:
            next_max_id = ''
        _ = api.getUserFollowings(user_id, maxid=next_max_id)
        js = api.LastJson
        for i in range(len(js['users'])):
            try:
                nickname = js['users'][i]['username']
                followings.append(nickname)
                if (cnt <= len(followings)):
                    return followings
            except:
                pass
        time.sleep(1.75)
        next_max_id = api.LastJson.get('next_max_id', '')
    return followings

def amqpConnect ():
    credentials = pika.PlainCredentials(config.AMQP_USER, config.AMQP_PASSWORD)
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=config.AMQP_HOST, port=config.AMQP_PORT, virtual_host=config.AMQP_VHOST,
                                  credentials=credentials))
    channel = connection.channel()

    channel.queue_declare(queue=config.AMQP_QUEUE_TASK, durable=True)
    channel.queue_declare(queue=config.AMQP_QUEUE_RESULT, durable=True)

    channel.exchange_declare(exchange=config.AMQP_EXCHANGE,
                             exchange_type='direct',
                             durable = True)

    channel.basic_consume(callback,
                          queue=config.AMQP_QUEUE_TASK)

    return channel


if __name__ == "__main__":
    username = ig_config.IG_LOGIN
    password = ig_config.IG_PASSWORD
    logging.basicConfig(filename="/home/endemic/work/php/giveaway/logs/python-scraper.log", level=logging.INFO)
    logging.info('Start')
    api = InstagramAPI(username, password)
    time.sleep(3.5)
    if ig_config.PROXY:
        api.setProxy(proxy=ig_config.PROXY)
    if (api.login()):
        api.getSelfUserFeed()  # get self user feed
        logging.info("Login success!")
    else:
        logging.error("Can't login!")
        quit()
    path_acc    = config.DATA_ACCOUNT_PATH
    path_post   = config.DATA_POST_PATH
    channel     = amqpConnect()
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
        connection.close()