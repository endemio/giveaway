<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 04.01.2019
 * Time: 16:15
 */

namespace App\Consumer;

use App\Controller\MainController;
use App\Service\CommentService;
use App\Service\GiveawayService;
use App\Service\PostService;
use App\Service\WinnerService;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class Instagram extends MainController implements ConsumerInterface {

    private $giveaway;

    private $winner;

    private $logger;

    private $comments;

    private $post;

    public function __construct(
        GiveawayService $giveawayService,
        WinnerService $winnerService,
        PostService $postService,
        CommentService $commentService,
        LoggerInterface $logger
    ){
        $this->winner = $winnerService;
        $this->giveaway = $giveawayService;
        $this->logger = $logger;
        $this->post = $postService;
        $this->comments = $commentService;
    }

    /**
     * @param AMQPMessage $msg
     * @return mixed|void
     * @throws \Exception
     */
    public function execute(AMQPMessage $msg) {

        print $msg->getBody().PHP_EOL;

        $this->logger->info('['.__FUNCTION__.'] Body '.$msg->getBody());

        #{"token":"nYkPdc5aM8","account":"whoosiskitty","task":"following","result":"complete","error":"none"}

        $body = json_decode($msg->getBody());

        $action = !empty($body->task)?$body->task:null;

        $token = !empty($body->token)?$body->token:null;

        $account = !empty($body->account)?$body->account:null;

        if (!empty($action)) {

            switch ($action){

                #{"giveaway":{},"task":"scrape-comments","date":{},"error":none}

                case 'scrape-comments':

                    $path = $body->giveaway;

                    $post = $this->post->listPostsByPath([$path])[0];

                    if ($post['comments_limit'] != 0) {
                        $post['limit'] = min($post['comments_limit'], $post['comments_post']) - $post['comments_get'];
                    } else {
                        $post['limit'] = $post['comments_post'] - $post['comments_get'];
                    }

                    $post['limit'] = ($post['limit'] > 0 )?$post['limit']:0;

                    $this->logger->info('Post data',$post);

                    if ($post['limit']) {

                        $response = $this->comments->getLongCommentsByPostsCode([$post]);

                        $this->logger->info('Response',$response);

                        $counter = $this->comments->storeComments($response['comments']);

                        $this->logger->info('Stored comments',$counter);

                        if (!empty($response['comments'])){
                            $this->post->updatePostEntityFromResponse($response);
                        }

                        foreach ($response['posts'] as $item) {

                            $this->logger->info('Post item',$item);

                            if (!empty($item['complete']) && $item['complete']) {

                                // Update Post status
                                #$this->post->updatePostStatusByGiveaway($item['giveaway'],1);

                                $this->logger->info(json_encode($item));

                                #$options = $this->giveaway->scrapingCommentsComplete($item['giveaway'],$post['comments_get']);
                                $options = $this->giveaway->scrapingCommentsComplete($item['giveaway'],$post['comments_get']+$counter[$item['giveaway']]);

                                $this->winner->chooseWinner($item['giveaway'], $options);
                            } else {
                                throw new \Exception('Throw exception to restart comments scraping for long comments list');
                            }
                        }
                    } else {

                        $this->post->updatePostStatusByGiveaway($path,1);

                        $options = $this->giveaway->scrapingCommentsComplete($path,$post['comments_get']);

                        $this->winner->chooseWinner($path, $options);

                    }

                    break;

                case 'following':

                    if (!empty($account)){

                        $this->logger->info('['.__FUNCTION__.'] Action - following ');

                        $filename = self::PythonScriptResultFolder . 'accounts/' . $account . '-following.data';

                        $this->logger->info('['.__FUNCTION__.'] Filename = '.$filename);

                        if (is_file($filename)) {

                            $instagram = explode(PHP_EOL, file_get_contents($filename));

                            $instagram = array_filter($instagram);

                            $this->logger->info('['.__FUNCTION__.'] Following number = '.count($instagram));

                            $giveaway = $this->giveaway->findGiveawayByUniqueCode($token);

                            $options = $giveaway->getOptions();

                            $following = explode(',',$options['follow']);

                            $result = 1;
                            if (!empty($following)) {
                                foreach ($following as $item) {
                                    $result = $result * in_array($item, $instagram);
                                }
                            }

                            try {

                                if (!$result){

                                    $this->logger->info('['.__FUNCTION__.'] Not following '.$account);

                                    $this->winner->updateWinner($token, $account, [['name' => 'complete', 'value' => $result]]);

                                    $this->winner->chooseWinner($token, $options);

                                } else {

                                    $this->logger->info('['.__FUNCTION__.'] Following ');

                                    $this->winner->updateWinner($token,  $account, [
                                        ['name' => 'follow', 'value' => true],
                                        ['name' => 'complete', 'value' => true]
                                    ]);

                                    $complete = $this->winner->checkCompleteChooseWinner($token,$account,$options);

                                    if ($complete){

                                        $this->giveaway->updateGiveawayStatusByUnique($token,3);

                                        $this->giveaway->sendNotificationGiveawayComplete($token,$account);

                                    }
                                }

                            } catch (\Exception $exception){
                                $this->logger->error($exception->getMessage());
                                throw new \Exception('['.__FUNCTION__.'] '.$exception->getMessage());
                            }
                        } else {

                            $this->winner->updateWinner($token, $account, [['name' => 'complete', 'value' => false]]);

                            $giveaway = $this->giveaway->findGiveawayByUniqueCode($token);

                            $options = $giveaway->getOptions();

                            $this->winner->chooseWinner($token, $options);

                            $this->logger->error($filename.' not exists');
                            //throw new \Exception($filename.' not exists');

                        }
                    }

                    break;

                case 'follow':

                    print_r($body);

                    break;


            }
        }
    }
}