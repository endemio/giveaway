<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 14.12.2018
 * Time: 5:08
 */

namespace App\Controller;

use App\Service\CommentService;
use App\Service\GiveawayService;
use App\Service\JSONService;
use App\Service\ParserService;
use App\Service\PostService;
use App\Service\PublisherService;
use App\Service\VoucherService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class APIController extends MainController{

    private $parser;

    private $JSON;

    private $comment;

    public function __construct(ParserService $parser, JSONService $JSON, CommentService $comment) {

        $this->parser = $parser;

        $this->JSON = $JSON;

        $this->comment = $comment;
    }

    /**
     * @Route("/api/check-voucher",methods={"POST"})
     * @param Request $request
     * @param VoucherService $voucherService
     * @return JsonResponse
     */
    public function checkVoucherData(
        Request $request,
        VoucherService $voucherService
    ):JsonResponse{

        $voucher = $request->request->get('voucher');

        $voucher_entity = $voucherService->findVoucherByUniqueCode($voucher);

        if (!empty($voucher_entity)){
            return new JsonResponse(['voucher' => $voucher, 'status' => $voucher_entity->getStatus()], 200);
        } else {
            return new JsonResponse(['voucher' => $voucher, 'status' => false], 200);
        }
    }

    /**
     * @Route("/api/check-giveaway-data/{code}",methods={"GET"})
     * @param string $code
     * @param CommentService $commentService
     * @return JsonResponse
     */
    public function checkGiveawayData(
        string $code,
        CommentService $commentService
    ):JsonResponse{

        $data = [];

        $number = $commentService->countCommentsByGiveawayCode($code);

        return new JsonResponse(['data'=>$data,'comments'=>$number],200);
    }

    /**
     * @Route("/api/get-post-data",methods={"POST"})
     * @param Request $request
     * @param PostService $postService
     * @return JsonResponse
     */
    public function getPostData(
        Request $request,
        PostService $postService
    ):JsonResponse{

        $code = $request->request->get('code');

        try {

            $post = $postService->getPostDataFromInstagramWebByCode($code);

            $result = [
                'caption'=>$post['caption'],
                'likes'=>$post['likes'],
                'username'=>$post['username'],
                'comments'=>$post['comments'],
                'profile_pic'=>$post['profile_pic'],
                'location'=>$post['location'],
                'image'=>$post['image']
            ];


        } catch (\Exception $exception){

            throw new HttpException(400,$exception->getMessage());

        }

        return new JsonResponse(['status'=>true,'post'=>$post],200);
    }

    /**
     * @Route("/api/start-giveaway",methods={"POST"})
     * @param Request $request
     * @param VoucherService $voucherService
     * @param CommentService $commentService
     * @param PostService $postService
     * @param GiveawayService $giveawayService
     * @return JsonResponse
     */
    public function startGiveaway(
        Request $request,
        VoucherService $voucherService,
        CommentService $commentService,
        PostService $postService,
        PublisherService $publisherService,
        GiveawayService $giveawayService
    ):JsonResponse{

        $csrf = $request->request->get('token');

        if (!$this->isCsrfTokenValid('csrf-start-giveaway', $csrf)) {
            //todo: uncomment
            #throw new HttpException(403,'CSRF error');
        }

        try {
            $voucher = $request->request->get('voucher');

            $code = $request->request->get('code');

            $options = (array)json_decode($request->request->get('options'));

        } catch (\Exception $exception){

            throw new HttpException(400,$exception->getMessage());

        }

        $limit = self::NonPremiumCommentsLimit;

        // Check voucher code
        if (!empty($voucher)){

            $voucher_entity = $voucherService->findVoucherByUniqueCode($voucher);

            // Voucher is false
            if ($voucher_entity == null ){

                $options = array_merge(self::NonPremiumOptionArray,['duplicates'=>$options["duplicates"]]);

                $voucher = 'none';

            } else {

                // Voucher has already used
                if ($voucher_entity->getStatus() > 1){

                    $options = array_merge(self::NonPremiumOptionArray,['duplicates'=>$options["duplicates"]]);

                    $voucher = 'none';

                // Voucher is correct
                } elseif ($voucher_entity->getStatus() == 1) {

                    //$voucherService->updateVoucherStatus($voucher_entity->getId(),2);

                    $limit = 0;
                }
            }
        } else {
            $options = array_merge(self::NonPremiumOptionArray,['duplicates'=>$options["duplicates"]]);
        }

        try {

            $limit = (!$options['allcomments'])?self::NonPremiumCommentsLimit:$limit;

            $post = $postService->getPostDataFromInstagramWebByCode($code);

            $path = $giveawayService->createGiveaway($code, $options,$post,$voucher);

            $comments = $postService->createPostByGiveaway($code,$path,$limit);

            $result = $commentService->storeComments([$path=>$comments]);

            $giveawayService->sendNotificationGiveawayStart($path,$options);

            $publisherService->sendRequestToStartGiveaway($path);

        } catch (\Exception $exception){

            throw new HttpException(400,'Create giveaway error '.$exception->getMessage());

        }

        return new JsonResponse(['status'=>true,'request'=>$request->request->all(),'options'=>$options,'path'=>$path],200);
    }

}