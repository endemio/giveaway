<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 01.01.2019
 * Time: 17:47
 */

namespace App\Controller;

use App\Service\CommentService;
use App\Service\GiveawayService;
use App\Service\PostService;
use App\Service\WinnerService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;


class AdminController extends MainController {

    /**
     * @Route("/api/admin/giveaways/list",methods={"POST"})
     * @param Request $request
     * @param GiveawayService $giveawayService
     * @return JsonResponse
     */
    public function listGiveaways(Request $request, GiveawayService $giveawayService,PostService $postService):JsonResponse {

        $page = $request->request->get('page');

        $giveaways = $giveawayService->listGiveawaysByPage($page);

        $posts = $postService->listPostsByPath(array_column($giveaways,'path'));

        return new JsonResponse(['list'=>$giveaways,'posts'=>$posts],200);

    }

    /**
     * @Route("/api/admin/giveaway/remove",methods={"POST"})
     * @param Request $request
     * @param GiveawayService $giveawayService
     * @param LoggerInterface $logger
     * @return JsonResponse
     */
    public function removeGiveaway (
        Request $request,
        PostService $postService,
        WinnerService $winnerService,
        CommentService $commentService,
        GiveawayService $giveawayService,
        LoggerInterface $logger
    ):JsonResponse{

        $giveaway_id = $request->request->get('id');

        $giveaway_entity = $giveawayService->findGiveawayById($giveaway_id);

        $giveaway = $giveaway_entity->getUniquePath();

        // remove Giveaway
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($giveaway_entity);
            $em->flush();
            $em->clear();
        } catch (\Exception $exception) {
            $logger->error('['.__FUNCTION__.'] Get error '.$exception->getMessage());
            throw new HttpException(400,$exception->getMessage());
        }

        // remove Post
        $postService->deletePostByGiveaway($giveaway);

        // remove Comments
        $commentService->deleteCommentsByGiveaway($giveaway);

        // remove Winner
        $winnerService->deleteWinnerByGiveaway($giveaway);


        return new JsonResponse(['error'=>null,'message'=>'Giveaway removed'],200,[]);

    }

}