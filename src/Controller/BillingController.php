<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 26.12.2018
 * Time: 8:31
 */

namespace App\Controller;

use Stripe\Stripe;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BillingController extends MainController {


    /**
     * @Route("/api/stripe/payment",methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function checkPayment(Request $request):JsonResponse{

        \Stripe\Stripe::setApiKey($this->getParameter('stripe_sec'));

        $customer = \Stripe\Customer::create([
            'email' => $request->request->get('email'),
            'source'  => $request->request->get('token'),
        ]);

        $charge = \Stripe\Charge::create([
            'customer' => $customer->id,
            'amount'   => 1000,
            'currency' => 'usd',
        ]);

        return new JsonResponse(['env'=>$this->getParameter('stripe_pub'),'charge'=>$charge]);
    }

}