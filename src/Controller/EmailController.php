<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 22.12.2018
 * Time: 12:46
 */

namespace App\Controller;


class EmailController {

    private $mailer;

    private $twig;

    /**
     * EmailController constructor.
     * @param \Swift_Mailer $mailer
     * @param \Twig_Environment $twig
     */
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig) {
        $this->mailer = $mailer;

        $this->twig = $twig;
    }

    /**
     * @param $template
     * @param $sender
     * @param $recipients
     * @param $subject
     * @param $data
     * @throws \Exception
     */
    public function sendContactMail($template,$sender,$recipients,$subject,$data){

        try {
            $body =  $this->twig->render(
                $template,
                $data
            ) ;
        } catch (\Exception $exception){
            throw new \Exception('['.__FUNCTION__.'] '.$exception->getMessage());
        }

        $message = (new \Swift_Message($subject))
            ->setFrom($sender)
            ->setTo($recipients)
            ->setBody(
                $body,
                'text/html'
            )
        ;
        $this->mailer->send($message);
    }
}