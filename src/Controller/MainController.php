<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 13.12.2018
 * Time: 4:15
 */
declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{

    public const InstagramAccountURL = 'https://www.instagram.com/';

    public const InstagramPostURL = 'https://www.instagram.com/p/';

    public const LongCommentsRoundLimit = 5;

    public const NonPremiumCommentsLimit = 200;

    public const PythonScriptResultFolder = '/home/endemic/work/php/giveaway/data/python/';

    public const NonPremiumOptionArray = [
        "follow"=> null,
        "phrase"=> null,
        "repost"=> false,
        "allcomments"=>false
    ];

    public function __construct(){

    }

}