<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 13.12.2018
 * Time: 4:17
 */

namespace App\Controller;

use App\Service\GiveawayService;
use App\Service\PostService;
use App\Service\WinnerService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends MainController
{

    /**
     * @Route("/",methods={"GET"},name="index")
     * @return Response
     */
    public function index (){

        try {
            return $this->render('index.html.twig',
                ['data' => []]
            );
        } catch (\Exception $exception){
            throw new HttpException(400,$exception->getMessage());
        }
    }

    /**
     * @Route("/giveaway/{code}",name="giveaway",methods={"GET"})
     * @param string $code
     * @param GiveawayService $giveawayService
     * @return Response
     */
    public function project (
        string $code,
        GiveawayService $giveawayService,
        WinnerService $winnerService,
        PostService $postService
    ):Response{

        $giveaway = $giveawayService->findGiveawayByUniqueCode($code);

        $post = $postService->getPostEntityByGiveaway($code);

        if ($giveaway->getStatus() == 3) {
            $winner = $winnerService->getWinnerByGiveaway($code);
            $winner_arr = [
                'comment_profile_pic'=>$winner->getProfilePic(),
                'winner'=>$winner->getAccount(),
                'comment'=>$winner->getText(),
                'created'=>date('c',$winner->getCreated())
            ];
        } else {
            $winner_arr = [
                'comment_profile_pic'=>'',
                'winner'=>'',
                'comment'=>'',
                'created'=>''
            ];
        }

        $data = [
            'code' => $code,
            'post' => $giveaway->getCode(),
            'comments'=>$giveaway->getComments(),
            'likes'=>$giveaway->getLikes(),
            'voucher'=>$giveaway->getVoucher(),
            'profile_pic'=>$post->getProfilePic(),
            'owner'=>$post->getOwner(),
            'location'=>$post->getLocation(),
            'image_src'=>$post->getImageSrc(),
            'text'=>$post->getText(),
        ];

        $data = array_merge($data,$winner_arr);

        try {
            return $this->render('giveaway.html.twig', $data);
        } catch (\Exception $exception){
            throw new HttpException(400,$exception->getMessage());
        }
    }

    /**
     * @Route("/premium",name="premium",methods={"GET"})
     * @return Response
     */
    public function premium ():Response{

        try {
            return $this->render('premium.html.twig',
                [

                ]
            );
        } catch (\Exception $exception){
            throw new HttpException(400,$exception->getMessage());
        }
    }

    /**
     * @Route("/admin",name="vouchers",methods={"GET"})
     * @return Response
     */
    public function admin ():Response{

        try {
            return $this->render('admin.html.twig',
                [

                ]
            );
        } catch (\Exception $exception){
            throw new HttpException(400,$exception->getMessage());
        }
    }



}