<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 04.02.2019
 * Time: 1:28
 */

namespace App\Controller;

use App\Entity\Winner;
use App\Events\CommentsScrapingCompleteEvent;
use App\Events\GiveawayChooseWinnerCompleteEvent;
use App\Events\GiveawayChooseWinnerStartEvent;
use App\Service\PostService;
use App\Service\JSONService;
use App\Service\CommentService;
use App\Service\ParserService;
use App\Service\GiveawayService;
use App\Service\WinnerService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UpdateController extends MainController{

    /**
     * @Route("/api/update/check-giveaway-in-work",methods={"GET"})
     * @param PostService $postService
     * @param EventDispatcherInterface $dispatcher
     * @param WinnerService $winnerService
     * @param CommentService $commentService
     * @param ParserService $parserService
     * @param GiveawayService $giveawayService
     * @return JsonResponse
     * @throws \Exception
     */
    /*public function checkGiveaways(
        PostService $postService,
        EventDispatcherInterface $dispatcher,
        WinnerService $winnerService,
        CommentService $commentService,
        ParserService $parserService,
        GiveawayService $giveawayService
    ):JsonResponse{

        $giveaways = $giveawayService->listGiveawaysToParse();

        $paths = array_column($giveaways,'unique_path');

        if (empty($paths)) {
            return new JsonResponse(['giveaways'=>[]],200);
        }

        $posts = $postService->listPostsByPath($paths);

        foreach ($posts as $key=>$post){
            if (empty($post['comments_limit'])){
                $posts[$key]['limit'] = $posts[$key]['comments_post'] - $posts[$key]['comments_get'];
            } else {
                $posts[$key]['limit'] = $posts[$key]['comments_limit'] - $posts[$key]['comments_get'];
            }

            $posts[$key]['limit'] = ($posts[$key]['limit'] > 0 )?$posts[$key]['limit']:0;
        }

        // Get Data to find $query_hash
        // If we still not get "body" when add new post(s) -> get it

        $html = $parserService->getHTMLPageContent([self::InstagramPostURL . $posts[0]['code']]);

        $body = $html[0]['body'];

        $query_hash = $parserService->getQueryHash($body,'PostPageContainer');

        $round = (Int)(self::LongCommentsRoundLimit/count($giveaways));

        $response = $parserService->getLongCommentsListFromPost($posts,$query_hash,$round);

        $comments = $commentService->storeComments($response['comments']);

        foreach ($response['posts'] as $item){

            if (!empty($response['comments'])){
                $postService->updatePostEntity($response);
            }

            if (!empty($item['complete']) && $item['complete']){

                // Update Post status
                $postService->updatePostStatusByGiveaway($item['giveaway'],1);

                // We have complete get comments from this giveaway
                $giveawayService->updateGiveawayStatusByUnique($item['giveaway'],2);

                // Start find winner
                $giveaway = $giveawayService->findGiveawayByUniqueCode($item['giveaway']);

                $giveawayService->sendNotificationGiveawayStartChooseWinner($item['giveaway'],$giveaway->getOptions());

                $winner = $winnerService->chooseWinner($item['giveaway'],$giveaway->getOptions());
            }
        }

        return new JsonResponse(['giveaways'=>$giveaways,'path'=>$paths,'posts_start'=>$posts,'added_comments'=>$comments,'response'=>$response],200);
    }*/

    //todo: start choose winner

    /**
     * @Route("/api/update/find-winner",methods={"GET"})
     * @param GiveawayService $giveawayService
     * @param WinnerService $winnerService
     * @param EventDispatcherInterface $dispatcher
     * @return JsonResponse
     */
    public function findWinner(
        GiveawayService $giveawayService,
        WinnerService $winnerService,
        EventDispatcherInterface $dispatcher
    ):JsonResponse {

        $path = 'CMiYmOo_qv';

        $giveaway = $giveawayService->findGiveawayByUniqueCode($path);

        $options = $giveaway->getOptions();

        $winner = $winnerService->chooseWinner($giveaway->getUniquePath(),$giveaway->getOptions());

        return new JsonResponse(['action'=>'choose winner','options'=>$options,'giveaway'=>$giveaway->getUniquePath()]);

    }
}