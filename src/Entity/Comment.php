<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 */
class Comment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $code;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $owner;

    /**
     * @ORM\Column(type="integer")
     */
    private $unixtime;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $unique_key;

    /**
     * @ORM\Column(type="integer")
     */
    private $Created;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $profile_pic;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getOwner(): ?string
    {
        return $this->owner;
    }

    public function setOwner(string $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getUnixtime(): ?int
    {
        return $this->unixtime;
    }

    public function setUnixtime(int $unixtime): self
    {
        $this->unixtime = $unixtime;

        return $this;
    }

    public function getUniqueKey(): ?string
    {
        return $this->unique_key;
    }

    public function setUniqueKey(string $unique_key): self
    {
        $this->unique_key = $unique_key;

        return $this;
    }

    public function getCreated(): ?int
    {
        return $this->Created;
    }

    public function setCreated(int $Created): self
    {
        $this->Created = $Created;

        return $this;
    }

    public function getProfilePic(): ?string
    {
        return $this->profile_pic;
    }

    public function setProfilePic(string $profile_pic): self
    {
        $this->profile_pic = $profile_pic;

        return $this;
    }
}
