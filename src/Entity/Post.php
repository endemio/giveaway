<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $code;

    /**
     * @ORM\Column(type="integer")
     */
    private $created;

    /**
     * @ORM\Column(type="integer")
     */
    private $checked;

    /**
     * @ORM\Column(type="integer")
     */
    private $comments_post;

    /**
     * @ORM\Column(type="integer")
     */
    private $comments_get;

    /**
     * @ORM\Column(type="integer")
     */
    private $likes;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $account_id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $after;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $gis;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $giveaway_path;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $comments_limit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image_src;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $owner;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $profile_pic;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getCreated(): ?int
    {
        return $this->created;
    }

    public function setCreated(int $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getChecked(): ?int
    {
        return $this->checked;
    }

    public function setChecked(int $checked): self
    {
        $this->checked = $checked;

        return $this;
    }

    public function getCommentsPost(): ?int
    {
        return $this->comments_post;
    }

    public function setCommentsPost(int $comments_post): self
    {
        $this->comments_post = $comments_post;

        return $this;
    }

    public function getCommentsGet(): ?int
    {
        return $this->comments_get;
    }

    public function setCommentsGet(int $comments_get): self
    {
        $this->comments_get = $comments_get;

        return $this;
    }

    public function getLikes(): ?int
    {
        return $this->likes;
    }

    public function setLikes(int $likes): self
    {
        $this->likes = $likes;

        return $this;
    }

    public function getAccountId(): ?string
    {
        return $this->account_id;
    }

    public function setAccountId(string $account_id): self
    {
        $this->account_id = $account_id;

        return $this;
    }

    public function getAfter(): ?string
    {
        return $this->after;
    }

    public function setAfter(string $after): self
    {
        $this->after = $after;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getGis(): ?string
    {
        return $this->gis;
    }

    public function setGis(string $gis): self
    {
        $this->gis = $gis;

        return $this;
    }

    public function getGiveawayPath(): ?string
    {
        return $this->giveaway_path;
    }

    public function setGiveawayPath(string $giveaway_path): self
    {
        $this->giveaway_path = $giveaway_path;

        return $this;
    }

    public function getCommentsLimit(): ?int
    {
        return $this->comments_limit;
    }

    public function setCommentsLimit(?int $comments_limit): self
    {
        $this->comments_limit = $comments_limit;

        return $this;
    }

    public function getImageSrc(): ?string
    {
        return $this->image_src;
    }

    public function setImageSrc(string $image_src): self
    {
        $this->image_src = $image_src;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getOwner(): ?string
    {
        return $this->owner;
    }

    public function setOwner(string $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getProfilePic(): ?string
    {
        return $this->profile_pic;
    }

    public function setProfilePic(string $profile_pic): self
    {
        $this->profile_pic = $profile_pic;

        return $this;
    }
}
