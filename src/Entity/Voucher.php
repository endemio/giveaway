<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VoucherRepository")
 */
class Voucher
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $code;

    /**
     * @ORM\Column(type="integer")
     */
    private $giveaway_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $unixtime;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getGiveawayId(): ?int
    {
        return $this->giveaway_id;
    }

    public function setGiveawayId(int $giveaway_id): self
    {
        $this->giveaway_id = $giveaway_id;

        return $this;
    }

    public function getUnixtime(): ?int
    {
        return $this->unixtime;
    }

    public function setUnixtime(int $unixtime): self
    {
        $this->unixtime = $unixtime;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
