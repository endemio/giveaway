<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WinnerRepository")
 */
class Winner
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $giveaway;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $account;

    /**
     * @ORM\Column(type="boolean")
     */
    private $liked;

    /**
     * @ORM\Column(type="boolean")
     */
    private $follow;

    /**
     * @ORM\Column(type="boolean")
     */
    private $repost;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $complete;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $profile_pic;

    /**
     * @ORM\Column(type="integer")
     */
    private $created;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGiveaway(): ?string
    {
        return $this->giveaway;
    }

    public function setGiveaway(string $giveaway): self
    {
        $this->giveaway = $giveaway;

        return $this;
    }

    public function getAccount(): ?string
    {
        return $this->account;
    }

    public function setAccount(string $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getLiked(): ?bool
    {
        return $this->liked;
    }

    public function setLiked(bool $liked): self
    {
        $this->liked = $liked;

        return $this;
    }

    public function getFollow(): ?bool
    {
        return $this->follow;
    }

    public function setFollow(bool $follow): self
    {
        $this->follow = $follow;

        return $this;
    }

    public function getRepost(): ?bool
    {
        return $this->repost;
    }

    public function setRepost(bool $repost): self
    {
        $this->repost = $repost;

        return $this;
    }

    public function getComplete(): ?bool
    {
        return $this->complete;
    }

    public function setComplete(?bool $complete): self
    {
        $this->complete = $complete;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getProfilePic(): ?string
    {
        return $this->profile_pic;
    }

    public function setProfilePic(string $profile_pic): self
    {
        $this->profile_pic = $profile_pic;

        return $this;
    }

    public function getCreated(): ?int
    {
        return $this->created;
    }

    public function setCreated(int $created): self
    {
        $this->created = $created;

        return $this;
    }
}
