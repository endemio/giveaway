<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 22.12.2018
 * Time: 5:48
 */

namespace App\Events;

use Symfony\Component\EventDispatcher\Event;

class CommentsScrapingCompleteEvent extends Event {

    const NAME = 'event.giveaway.scraping.complete';

    protected $unique;

    public function setGiveaway($unique){

        $this->unique = $unique;
    }

    public function getGiveaway(){

        return $this->unique;
    }

    protected $counter;

    public function setCounter($counter){

        $this->counter = $counter;
    }

    public function getCounter(){

        return $this->counter;
    }    
    
}