<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 22.12.2018
 * Time: 5:48
 */

namespace App\Events;

use Symfony\Component\EventDispatcher\Event;

class CommentsScrapingStartEvent extends Event {

    const NAME = 'event.giveaway.scraping.complete';

    protected $account;

    public function setFoo($username){

        $this->account = $username;
    }

    public function getFoo(){

        return $this->account;
    }

}