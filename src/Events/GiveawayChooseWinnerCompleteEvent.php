<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 09.01.2019
 * Time: 11:06
 */

namespace App\Events;


use Symfony\Component\EventDispatcher\Event;

class GiveawayChooseWinnerCompleteEvent extends Event {

    const NAME = 'event.giveaway.choose.winner.complete';

    protected $path;

    protected $winner;

    public function setGiveaway($path){

        $this->path = $path;
    }

    public function getGiveaway(){

        return $this->path;
    }

    public function setWinner($winner){

        $this->winner = $winner;
    }

    public function getWinner(){

        return $this->winner;
    }
}