<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 09.01.2019
 * Time: 11:06
 */

namespace App\Events;

use Symfony\Component\EventDispatcher\Event;

class GiveawayChooseWinnerStartEvent extends Event {

    const NAME = 'event.giveaway.choose.winner.start';

    protected $path;

    protected $options;

    public function setGiveaway(string $path){

        $this->path = $path;
    }

    public function setOptions(array $options){

        $this->options = $options;
    }

    public function getGiveaway(){

        return $this->path;
    }

    public function getOptions(){

        return $this->options;
    }
}