<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 09.01.2019
 * Time: 11:06
 */

namespace App\Events;

use Symfony\Component\EventDispatcher\Event;

class GiveawayError extends Event {

    const NAME = 'event.giveaway.choose.winner.error';

    protected $path;

    protected $options;

    protected $message;

    public function setGiveaway(string $path){

        $this->path = $path;
    }

    public function setOptions(array $options){

        $this->options = $options;
    }

    public function setMessage(string $message){

        $this->message = $message;
    }

    public function getGiveaway(){

        return $this->path;
    }

    public function getOptions(){

        return $this->options;
    }

    public function getMessage(){

        return $this->message;
    }

}