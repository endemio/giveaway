<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 09.01.2019
 * Time: 11:01
 */

namespace App\Events;

use Symfony\Component\EventDispatcher\Event;

class GiveawayStartEvent extends Event {

    const NAME = 'event.giveaway.start';

    protected $path;

    protected $options;

    public function setPath($path){

        $this->path = $path;
    }

    public function setOptions($options){

        $this->options = $options;
    }

    public function getPath(){

        return $this->path;
    }

    public function getOptions(){

        return $this->options;
    }

}