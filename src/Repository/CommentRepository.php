<?php

namespace App\Repository;

use App\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    // /**
    //  * @return Comment[] Returns an array of Comment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Comment
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function deleteCommentsByGiveaway($giveaway){
        $builder = $this->createQueryBuilder('p')
            ->delete()
            ->where('p.code = :giveaway')
            ->setParameter('giveaway',$giveaway)
            ->getQuery();

        $builder->execute();
    }

    /**
     * @param string $code
     * @return array
     */
    public function giveawayStatus(string $code){

        $data = $this->createQueryBuilder('p')
            ->select('count(p.id) as number')
            ->where('p.code = :code')
            ->setParameter('code',$code)
            ->getQuery()
            ->getResult();

        return $data['0'];

    }

    public function getCommentsByGiveaway($giveaway){
        $comments = $this->createQueryBuilder('p')
            ->where('p.code = :code')
            ->setParameter('code',$giveaway)
            ->getQuery()
            ->getResult();

        $result = [];

        foreach ($comments as $comment){
            $result[] = $this->transform($comment);
        }

        return $result;
    }

    public function getCommentsUniqueKeysByPostCode(string $code){
        $comments = $this->findBy(['code'=>$code]);

        $result = [];

        foreach ($comments as $comment){
            array_push($result,$comment->getUniqueKey());
        }

        return $result;

    }

    private function transform(Comment $comment){
        return [
            'owner'=>$comment->getOwner(),
            'created'=>$comment->getCreated(),
            'text'=>$comment->getText(),
            'profile_pic'=>$comment->getProfilePic(),
        ];
    }


}
