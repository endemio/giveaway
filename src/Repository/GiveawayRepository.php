<?php

namespace App\Repository;

use App\Entity\Giveaway;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Giveaway|null find($id, $lockMode = null, $lockVersion = null)
 * @method Giveaway|null findOneBy(array $criteria, array $orderBy = null)
 * @method Giveaway[]    findAll()
 * @method Giveaway[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GiveawayRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Giveaway::class);
    }

    // /**
    //  * @return Giveaway[] Returns an array of Giveaway objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Giveaway
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function listGiveawayByPage(int $page){
        $giveaways = $this->createQueryBuilder('p')
            //->setFirstResult(($page-1)*20)
            //->setMaxResults($page*20)
            ->getQuery()
            ->getResult();


        $result = [];

        foreach ($giveaways as $giveaway){
            $result[] = $this->transform($giveaway);
        }

        return $result;
    }

    private function transform(Giveaway $giveaway){
        return [
            'id'=>$giveaway->getId(),
            'user_id'=>$giveaway->getUserId(),
            'path'=>$giveaway->getUniquePath(),
            'code'=>$giveaway->getCode(),
            'voucher'=>$giveaway->getVoucher(),
            'comments'=>$giveaway->getComments(),
            'likes'=>$giveaway->getLikes(),
            'unique_path'=>$giveaway->getUniquePath()
        ];
    }

    public function listGiveawayToParse(){
        $ga = $this->createQueryBuilder('p')
            ->where('p.status = :status')
            ->setParameter('status',1)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($ga as $element){
            $result[] = $this->transform($element);
        }

        return $result;
    }

}
