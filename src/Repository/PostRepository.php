<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function deletePostByGiveaway($giveaway){
        $builder = $this->createQueryBuilder('p')
            ->delete()
            ->where('p.giveaway_path = :giveaway')
            ->setParameter('giveaway',$giveaway)
            ->getQuery();

        $builder->execute();
    }

    public function getPostsToParse(int $number){
        $posts = $this->createQueryBuilder('p')
            ->where('p.status = :status')
            ->setParameter('status',1)
            ->setMaxResults($number)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ( $posts as $post){
            $result[] = $this->transform($post);
        }

        return $result;
    }

    public function listPostsByCodes(array $codes){
        $posts = $this->createQueryBuilder('p')
            ->where('p.code IN(:codes)')
            ->setParameter('codes',array_values($codes))
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ( $posts as $post){
            $temp = $this->transform($post);
            $result[$temp['code']] = $temp;
        }

        return $result;
    }


    public function listPostsByPaths(array $paths){
        $posts = $this->createQueryBuilder('p')
            ->where('p.giveaway_path IN(:paths)')
            ->setParameter('paths',array_values($paths))
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ( $posts as $post){
            $result[] = $this->transform($post);
        }

        return $result;
    }

    private function transform(Post $post){
        return [
            'id'=>$post->getId(),
            'status'=>$post->getStatus(),
            'code'=>$post->getCode(),
            'after'=>$post->getAfter(),
            'gis'=>$post->getGis(),
            'get'=>$post->getCommentsGet(),
            'giveaway'=>$post->getGiveawayPath(),
            'comments_get'=>$post->getCommentsGet(),
            'comments_post'=>$post->getCommentsPost(),
            'comments_limit'=>$post->getCommentsLimit(),
        ];
    }

}
