<?php

namespace App\Repository;

use App\Entity\Winner;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Winner|null find($id, $lockMode = null, $lockVersion = null)
 * @method Winner|null findOneBy(array $criteria, array $orderBy = null)
 * @method Winner[]    findAll()
 * @method Winner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WinnerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Winner::class);
    }

    // /**
    //  * @return Giveaway[] Returns an array of Giveaway objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Giveaway
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function deleteEntityByGiveaway($giveaway){

        $builder = $this->createQueryBuilder('p')
            ->delete()
            ->where('p.giveaway = :giveaway')
            ->setParameter('giveaway',$giveaway)
            ->getQuery();

        $builder->execute();

    }

    public function getListByGiveaway($giveaway){
        $list = $this->createQueryBuilder('p')
            ->where('p.giveaway = :giveaway')
            ->setParameter('giveaway',$giveaway)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($list as $item){
            $result[] = $this->transform($item);
        }

        return $result;
    }

    private function transform(Winner $winner){
        return [
            'account'=>$winner->getAccount()
        ];
    }

}
