<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 01.01.2019
 * Time: 18:01
 */

namespace App\Service;

use App\Controller\MainController;
use App\Entity\Comment;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class CommentService extends MainController {

    private $JSON;

    private $em;

    private $logger;

    private $parser;

    /**
     * CommentService constructor.
     * @param JSONService $JSON
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        JSONService $JSON,
        ParserService $parserService,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger
    ){

        $this->JSON = $JSON;

        $this->em = $entityManager;

        $this->logger = $logger;

        $this->parser = $parserService;

    }

    /**
     * @param array $commentsList
     * @return array
     * @throws \Exception
     */
    public function storeComments (array $commentsList):array {

        $counter = [];

        foreach ($commentsList as $key=>$comments) {

            $counter[$key] = 0;

            // Get comments list from DB
            $commentsDB = $this->em->getRepository(Comment::class)->getCommentsUniqueKeysByPostCode($key);

            foreach ($comments as $comment) {

                $comment_arr = $this->JSON->getCommentDataFromJSON($comment);

                $unique = $key.'_'.$comment_arr['id'];

                // If comments not in DB
                if (!in_array($unique, $commentsDB)) {
                    try {

                        $comment = new Comment();

                        $comment
                            ->setText($comment_arr['text'])
                            ->setCreated($comment_arr['created'])
                            ->setUnixtime(time())
                            ->setUserId(0)
                            ->setUniqueKey($unique)
                            ->setCode($key)
                            ->setProfilePic($comment_arr['profile_pic'])
                            ->setOwner($comment_arr['owner']);

                        $this->em->persist($comment);

                        array_push($commentsDB, $unique);

                        $counter[$key]++;

                    } catch (\Exception $exception) {

                        $this->logger->error('[' . __FUNCTION__ . '] Get error ' . $exception->getMessage());

                        throw new \Exception($exception->getMessage() . json_encode($commentsDB), 456);
                    }
                }
            }

            try {
                $this->em->flush();
                $this->em->clear();
            } catch (\Exception $exception) {
                $this->logger->error('[' . __FUNCTION__ . '] Get error ' . $exception->getMessage());
                throw new \Exception('[' . __FUNCTION__ . '] Get error ' . $exception->getMessage() . ' key=' . $key, 456);
            }
        }

        return $counter;
    }

    public function countCommentsByGiveawayCode(string $code){

        $comments = $this->getDoctrine()->getRepository(Comment::class)->giveawayStatus($code);

        return $comments['number'];
    }

    public function getAllCommentsByGiveaway(string $giveaway){

        $comments = $this->getDoctrine()->getRepository(Comment::class)->getCommentsByGiveaway($giveaway);

        return $comments;
    }

    public function deleteCommentsByGiveaway(string $giveaway){

        $this->getDoctrine()->getRepository(Comment::class)->deleteCommentsByGiveaway($giveaway);

    }

    /**
     * @param array $posts
     * @return mixed
     * @throws \Exception
     */
    public function getLongCommentsByPostsCode(array $posts){

        try {
            $html = $this->parser->getHTMLPageContent([self::InstagramPostURL . $posts[0]['code']]);

            $body = $html[0]['body'];

            $query_hash = $this->parser->getQueryHash($body, 'PostPageContainer');

            $round = (Int)(self::LongCommentsRoundLimit);

            $response = $this->parser->getLongCommentsListFromPost($posts, $query_hash, $round);

            return $response;

        } catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }
    }

}