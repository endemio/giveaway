<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 22.12.2018
 * Time: 12:46
 */

namespace App\Service;

use App\Controller\MainController;
use Psr\Log\LoggerInterface;

class EmailService extends MainController {

    private $mailer;

    private $twig;

    private $logger;

    /**
     * EmailService constructor.
     * @param LoggerInterface $logger
     * @param \Swift_Mailer $mailer
     * @param \Twig_Environment $twig
     */
    public function __construct(
        LoggerInterface $logger,
        \Swift_Mailer $mailer,
        \Twig_Environment $twig
    ) {
        $this->mailer = $mailer;

        $this->twig = $twig;

        $this->logger = $logger;
    }

    /**
     * @param $template
     * @param $sender
     * @param $recipients
     * @param $subject
     * @param $data
     * @throws \Exception
     */
    public function sendContactMail($template,$sender,$recipients,$subject,$data){

        $this->logger->info('Start sendContactMail');

        try {
            $body =  $this->twig->render(
                $template,
                $data
            ) ;
        } catch (\Exception $exception){

            $this->logger->error('['.__FUNCTION__.'] '.$exception->getMessage());
            throw new \Exception('['.__FUNCTION__.'] '.$exception->getMessage());
        }

        try {
            $message = (new \Swift_Message($subject))
                ->setFrom($sender)
                ->setTo($recipients)
                ->setBody(
                    $body,
                    'text/html'
                );
            $status = $this->mailer->send($message);
        } catch (\Exception $exception){

            $this->logger->error('['.__FUNCTION__.'] '.$exception->getMessage());
            throw new \Exception('['.__FUNCTION__.'] '.$exception->getMessage());
        }
    }
}