<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 07.01.2019
 * Time: 17:06
 */

namespace App\Service;


use App\Controller\MainController;
use App\Entity\Giveaway;
use App\Events\CommentsScrapingCompleteEvent;
use App\Events\GiveawayChooseWinnerCompleteEvent;
use App\Events\GiveawayError;
use App\Events\GiveawayChooseWinnerStartEvent;
use App\Events\GiveawayStartEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class GiveawayService extends MainController{

    private $logger;

    private $dispatcher;

    public function __construct(LoggerInterface $logger, EventDispatcherInterface $dispatcher) {

        $this->logger = $logger;

        $this->dispatcher = $dispatcher;

    }

    public function listGiveawaysByPage(int $page){

        $giveaways = $this->getDoctrine()->getRepository(Giveaway::class)->listGiveawayByPage($page);

        return $giveaways;
    }


    public function findGiveawayById(int $id){

        $giveaway = $this->getDoctrine()->getRepository(Giveaway::class)->find($id);

        return $giveaway;
    }

    public function findGiveawayByUniqueCode(string $code){

        $giveaway = $this->getDoctrine()->getRepository(Giveaway::class)->findOneBy(['UniquePath'=>$code]);

        return $giveaway;
    }

    public function listGiveawaysToParse(){

        $giveaways = $this->getDoctrine()->getRepository(Giveaway::class)->listGiveawayToParse();

        return $giveaways;

    }

    /**
     * @param string $code
     * @param array $options
     * @param array $post
     * @return bool|string
     * @throws \Exception
     */
    public function createGiveaway (string $code,array $options,array $post,$voucher) {

        try {
            $em = $this->getDoctrine()->getManager();
            // Store post code to DB
            $giveaway = new Giveaway();

            $unique = $this->generateRandomString(10);

            $giveaway
                ->setCode($code)
                ->setUserId(0)
                ->setComments(!empty($post['comments'])?$post['comments']:0)
                ->setLikes(!empty($post['likes'])?$post['likes']:0)
                ->setUniquePath($unique)
                ->setOptions($options)
                ->setStatus(1)
                ->setCreated(new \DateTime('now'))
                ->setVoucher(!empty($voucher) ? $voucher : '');

            $em->persist($giveaway);
            $em->flush();
            $em->clear();

            return $unique;
        } catch (\Exception $exception){
            $this->logger->error($exception->getMessage());
            throw new \Exception('['.__FUNCTION__.'] '.$exception->getMessage());
        }
    }

    public function updateGiveawayStatusByUnique($unique,$status){

        $em = $this->getDoctrine()->getManager();

        $giveaway = $em->getRepository(Giveaway::class)->findOneBy(['UniquePath'=>$unique]);

        $giveaway
            ->setStatus($status);

        $em->persist($giveaway);
        $em->flush();
        $em->clear();

    }






    private function generateRandomString($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_', (int)ceil($length/strlen($x)) )),1,$length);
    }


    /**
     * @param $giveaway
     * @param $winner
     */
    public function sendNotificationGiveawayComplete($giveaway,$winner){

        $this->logger->info('Giveaway '.$giveaway.' is complete');

        $event = new GiveawayChooseWinnerCompleteEvent();
        $event->setGiveaway($giveaway);
        $event->setWinner($winner);
        $this->dispatcher->dispatch(GiveawayChooseWinnerCompleteEvent::NAME,$event);

    }

    /**
     * @param $giveaway
     * @param $options
     * @param $message
     */
    public function sendNotificationGiveawayError($giveaway,$options,$message){

        $this->logger->info('Giveaway Error: '.$giveaway.' have got error by message '.$message);

        $event = new GiveawayError();
        $event->setGiveaway($giveaway);
        $event->setOptions($options);
        $event->setMessage($message);
        $this->dispatcher->dispatch(GiveawayError::NAME,$event);


    }

    public function sendNotificationGiveawayStartChooseWinner($giveaway,$options){

        $this->logger->info('Giveaway ' . $giveaway . ' start choose winner');

        // Notify that we have already complete scraping for this giveaway
        $event = new GiveawayChooseWinnerStartEvent();
        $event->setGiveaway($giveaway);
        $event->setOptions($options);
        $this->dispatcher->dispatch(GiveawayChooseWinnerStartEvent::NAME, $event);

    }

    public function sendNotificationGiveawayStart($giveaway,$options)  {

        $this->logger->info('Giveaway ' . $giveaway . ' start');

        // Send email as giveaway start
        $event = new GiveawayStartEvent();
        $event->setPath($giveaway);
        $event->setOptions($options);
        $this->dispatcher->dispatch(GiveawayStartEvent::NAME, $event);

    }


    public function scrapingCommentsComplete($path,$counter){

        // Notify that we have already complete scraping for this giveaway
        $event = new CommentsScrapingCompleteEvent();
        $event->setGiveaway($path);
        $event->setCounter($counter);
        $this->dispatcher->dispatch(CommentsScrapingCompleteEvent::NAME,$event);

        // We have complete get comments from this giveaway
        $this->updateGiveawayStatusByUnique($path,2);

        // Start find winner
        $giveaway = $this->findGiveawayByUniqueCode($path);

        $this->sendNotificationGiveawayStartChooseWinner($path,$giveaway->getOptions());

        return $giveaway->getOptions();

    }

}