<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 01.01.2019
 * Time: 17:59
 */

namespace App\Service;

use App\Controller\MainController;
use Psr\Log\LoggerInterface;

class JSONService extends MainController {

    private $logger;

    public function __construct(LoggerInterface $logger) {

        $this->logger = $logger;
    }

    /**
     * @param array $object
     * @return array
     * @throws \Exception
     */
    /*public function getCommentsDataFromObject(array $object){

        $comments = [];



        try {
            foreach ($object as $edge) {

                print_r($edge);

                /*array_push($comments, [
                    'comment' => $edge->{'node'}->{'text'},
                    'owner' => $edge->{'node'}->{'owner'}->{'username'},
                    'index' => $edge->{'node'}->{'id'}
                ]);
            }
            exit;
        }  catch (\Exception $exception) {
            throw new \Exception('['.__FUNCTION__.'] '.$exception->getMessage());

        }

        return $comments;
    }*/


    /**
     * Get post info from object
     * @param object $post
     * @return array
     * @throws \Exception
     */
    public function getPostDataFromJSON (object $post){

        try {

            $shortcode = $post->{'entry_data'}->{'PostPage'}[0]->{'graphql'}->{'shortcode_media'};

            $edge_media = $shortcode->{'edge_media_to_comment'};

        } catch (\Exception $exception) {
            throw new \Exception('['.__FUNCTION__.'] '.$exception->getMessage());

        }

        $caption = (!empty($shortcode->{'edge_media_to_caption'}->{'edges'}[0]->{'node'}->{'text'}))?$shortcode->{'edge_media_to_caption'}->{'edges'}[0]->{'node'}->{'text'}:'';


        try{
            return [
                'code'      =>$shortcode->{'shortcode'},
                'likes'     =>$shortcode->{'edge_media_preview_like'}->{'count'},
                'comments'  =>$shortcode->{'edge_media_to_comment'}->{'count'},
                'image'     =>$shortcode->{'display_resources'}[0]->{'src'},
                'username'  =>$shortcode->{'owner'}->{'username'},
                'profile_pic'=>$shortcode->{'owner'}->{'profile_pic_url'},
                'location'  =>!empty($shortcode->{'location'}->{'name'})?$shortcode->{'location'}->{'name'}:'',
                'caption'   =>$caption,
                'rhx_gis'   =>$post->{'rhx_gis'},
                'comments_list'=>!empty($edge_media->{'edges'})?$edge_media->{'edges'}:[],
                'after'     =>$edge_media->{'page_info'}->{'end_cursor'},
                'has_query' =>$edge_media->{'page_info'}->{'has_next_page'}
            ];
        } catch (\Exception $exception){
            throw new \Exception($exception->getMessage().' post '.json_encode($post),433);
        }
    }

    /**
     * @param object $comment
     * @return array
     * @throws \Exception
     */
    public function getCommentDataFromJSON (object $comment){

        try {
            $node = !empty($comment->{'node'})?$comment->{'node'}:$comment;
        } catch (\Exception $exception) {
            throw new \Exception('['.__FUNCTION__.'] '.$exception->getMessage());
        }

        try{
            return [
                'id'=>$node->{'id'},
                'text'=>$node->{'text'},
                'created'=>$node->{'created_at'},
                'profile_pic'=>$node->{'owner'}->{'profile_pic_url'},
                'owner'=>$node->{'owner'}->{'username'}
            ];
        } catch (\Exception $exception){
            throw new \Exception($exception->getMessage().' post '.json_encode($comment),433);
        }
    }

    /**
     * @param string $html
     * @return string
     * @throws \Exception
     */
    public function getJsonFromHtml (string $html):string {

        try {
            // Parse Htm for JSON part
            preg_match('/<script type=\"text\/javascript\">window._sharedData = (.*);<\/script>/', $html, $matches);
            if (!empty($matches[1])) {
                return $matches[1];
            } else{
                $this->logger->error('['.__FUNCTION__.'] Get error -> haven`t got matches');
                throw new \Exception('Get error -> haven`t got matches',400);
            }
        } catch (\Exception $exception) {
            $this->logger->error('['.__FUNCTION__.'] Get error '.$exception->getMessage());
            throw new \Exception($exception->getMessage(),422);
        }

    }
}