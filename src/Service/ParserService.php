<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 01.01.2019
 * Time: 18:06
 */

namespace App\Service;

use App\Controller\MainController;
use Clue\React\HttpProxy\ProxyConnector;
use Clue\React\Block;
use Clue\React\Buzz\Browser;
use Psr\Log\LoggerInterface;
use React\EventLoop\Factory;
use React\EventLoop\LoopInterface;
use React\Socket\Connector;
use Psr\Http\Message\ResponseInterface;

class ParserService extends MainController {

    private $logger;

    public function __construct(LoggerInterface $logger){

        $this->logger = $logger;

    }

    public function getHTMLPageContent(array $urls,$useragent='',$headers=[],$proxyData=[]){

        $loop = Factory::create();

        if (!empty($proxyData)) {
            $proxy = new ProxyConnector(
                'http://' . $proxyData['user'] . ':' . $proxyData['pass'] . '@' . $proxyData['host'] . ':' . $proxyData['port'],
                new Connector($loop)
            );
            $conn = ['tcp'=>$proxy,'timeout'=>5.0,'dns'=>false ];
        } else {
            $conn = ['timeout'=>5.0,'dns' => true];
        }

        $connector = new Connector($loop,$conn);

        $client = new Browser($loop,$connector);

        $promises = [];

        $header = [
            'User-agent' =>     (!empty($useragent))?$useragent:'',
            'Accept-Language'=> 'en-us,en;q=0.5',
            'Accept-Charset'=> 'ISO-8859-1,utf-8;q=0.7,*;q=0.7',
        ];

        $header = array_merge($header,$headers);

        $requests = [];
        foreach ($urls as $url){
            $requests[] = [
                'url' => $url,
                'header' => $header
            ];
        }

        $responses = $this->getJSONContentFromRequest($client, $loop, $requests);

        return $responses;
    }

    private function getJSONContentFromRequest(
        Browser $client,
        LoopInterface $loop,
        $requests
    ){

        $promises = [];

        foreach ($requests as $request){

            $this->logger->info('['.__FUNCTION__.'] Getting data from URL = '.$request['url']);

            array_push($promises,
                $client->get($request['url'],$request['header'])->then(
                    function (ResponseInterface $response) use ($request){
                        $result['error'] = false;
                        $result['shortcode'] = !empty($request['shortcode'])?$request['shortcode']:'';
                        $result['body'] = (string)$response->getBody();
                        $result['bytes'] = $response->getBody()->getSize();
                        return $result;
                    },
                    function (\Exception $exception){
                        $result['error'] = true;
                        $result['body'] = '';
                        $result['bytes'] = 0;
                        $result['message'] = $exception->getMessage();
                        $result['shortcode'] = !empty($request['shortcode'])?$request['shortcode']:'';
                        $result['code'] = $exception->getCode();
                        $this->logger->error('['.__FUNCTION__.'] Get error '.$exception->getMessage());
                        return $result;
                    }
                )
            );
        }

        try{
            $results = Block\awaitAll($promises,$loop);
            return $results;
        } catch (\Exception $exception){
            return ['error'=>true,'message'=>$exception->getMessage()];
        }
    }

    /**
     * @param array $posts
     * @param string $query_hash
     * @param int $limit
     * @param string $useragent
     * @return array
     */
    public function getLongCommentsListFromPost(array $posts,string $query_hash, int $limit, string $useragent = ''):array{

        $loop = Factory::create();

        if (!empty($proxyData)) {
            $proxy = new ProxyConnector(
                'http://' . $proxyData['user'] . ':' . $proxyData['pass'] . '@' . $proxyData['host'] . ':' . $proxyData['port'],
                new Connector($loop)
            );
            $conn = ['tcp'=>$proxy,'timeout'=>5.0,'dns'=>false ];
        } else {
            $conn = ['timeout'=>5.0,'dns' => true];
        }

        $connector = new Connector($loop,$conn);

        $client = new Browser($loop,$connector);

        $has_query = true;

        $comments = [];

        $round = 0;

        while($has_query) {

            $has_query = false;

            $requests = [];

            $link = [];

            $giveaway = [];

            $this->logger->info(__FUNCTION__.' Posts ',$posts);

            foreach ($posts as $key=>$post) {

                $comments[$post['giveaway']] = empty($comments[$post['giveaway']])?[]:$comments[$post['giveaway']];

                $link[$post['code']] = $key;

                $giveaway[$post['code']] = $post['giveaway'];

                // If we reach limit - stop getting comments for this post
                if ($post['limit'] == 0 || $post['limit'] < count($comments[$post['giveaway']])) {

                    $this->logger->info('!!! - Complete get comments '. json_encode($post).' Count = '.count($comments[$post['giveaway']]));

                    $posts[$key]['complete'] = true;
                }


                if (empty($posts[$key]['after'])){
                    $posts[$key]['complete'] = true;
                }


                if (!empty($post['after']) && !$post['status'] && empty($posts[$key]['complete'])) {

                    $has_query = true;

                    $variable = $this->getQueryCommentsVariable($post['code'], 40, $post['after']);

                    $gis = md5($post['gis'] . ':' . $variable);

                    $requests[] = [
                        'url' => 'https://www.instagram.com/graphql/query/?query_hash=' . $query_hash . '&variables=' . $variable,
                        'shortcode'=> $post['code'],
                        'header' => [
                            'User-agent' => $useragent,
                            'X-Instagram-GIS' => $gis,
                            'X-Requested-With' => 'XMLHttpRequest',
                        ]
                    ];
                }
            }


            $this->logger->info(__FUNCTION__.' Requests ',$requests);

            if (count($requests) > 0) {

                $responses = $this->getJSONContentFromRequest($client, $loop, $requests);

                foreach ($responses as $response) {

                    $this->logger->info(__FUNCTION__.' Response',$response);

                    try {

                        $object = json_decode($response['body']);

                        $edge_media_to_comment = $object->{'data'}->{'shortcode_media'}->{'edge_media_to_comment'};

                        $comments[$giveaway[$response['shortcode']]] = array_merge($comments[$giveaway[$response['shortcode']]], $edge_media_to_comment->{'edges'});

                        if (!empty($edge_media_to_comment->{'page_info'}->{'end_cursor'})) {
                            $posts[$link[$response['shortcode']]]['after'] = $edge_media_to_comment->{'page_info'}->{'end_cursor'};
                        } else {
                            $this->logger->info('!!! - Complete get comments ',(array)$edge_media_to_comment->{'page_info'});
                            $posts[$link[$response['shortcode']]]['complete'] = true;
                            $posts[$link[$response['shortcode']]]['after'] = '';
                        }

                    } catch (\Exception $exception) {


                    }
                }
            }

            // Stop parsing if we get more $round then $limit (for huge number of comments post)
            if ($round > $limit ) {
                $has_query = false;
            }

            $round++;

            sleep(1);
        }

        $this->logger->info('getLongCommentsListFromPost end '.json_encode($posts));

        return ['comments'=>$comments,'posts'=>$posts];
    }

    /**
     * @param string $html
     * @param string $container
     * @return mixed
     * @throws \Exception
     */
    public function getQueryHash(string $html,string $container){

        $pattern = '/\/static\/bundles\/metro\/'.$container.'\.js\/(.*?)\.js/';

        preg_match($pattern,$html,$matches);

        if (empty($matches)){
            throw new \Exception('JS file match not found',455);
        }

        $urls = ['https://www.instagram.com/static/bundles/metro/'.$container.'.js/'.$matches[1].'.js'];

        $content = $this->getHTMLPageContent($urls);

        if ($container == 'PostPageContainer') {
            $pattern = '/\(n\)\.pagination\}\,queryId\:\"(.*?)\"\,queryParams/';
        }

        if ($container == 'ProfilePageContainer') {
            $pattern = '/r\.pagination\}\,queryId\:\"(.*?)\"\,queryParams/';
        }

        try {
            preg_match($pattern, $content[0]['body'], $matches);
        } catch (\Exception $exception){
            throw new \Exception($exception->getMessage(),456);
        }

        if (!empty($matches[1])){
            return $matches[1];
        } else {
            throw new \Exception(print_r($matches,true),457);
        }
    }

    /**
     * @param string $code
     * @param int $limit
     * @param string $cursor
     * @return string
     */
    private function getQueryCommentsVariable(string $code, int $limit, string $cursor){
        return '{"shortcode":"'.$code.'","first":'.$limit.',"after":"'.$cursor.'"}';
    }
}