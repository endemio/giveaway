<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 07.01.2019
 * Time: 19:13
 */

namespace App\Service;


use App\Controller\MainController;
use App\Entity\Post;

class PostService extends MainController
{

    private $parser;

    private $json;

    public function __construct(
        JSONService $json,
        ParserService $parserService
    ){
        $this->parser = $parserService;

        $this->json = $json;
    }

    /**
     * @param $code
     * @return array
     * @throws \Exception
     */
    public function getPostDataFromInstagramWebByCode($code){

        if (empty($code) || strlen($code) !== 11){
            throw new \Exception('Wrong IG Post code');
        }

        $html = $this->parser->getHTMLPageContent([self::InstagramPostURL.$code]);


        try {

            $json = $this->json->getJsonFromHtml($html[0]['body']);

            $post = $this->json->getPostDataFromJSON(json_decode($json));

        } catch (\Exception $exception){

            throw new \Exception(__FUNCTION__.' '.$exception->getMessage());

        }

        return $post;
    }

    public function deletePostByGiveaway($giveaway){

        $this->getDoctrine()->getRepository(Post::class)->deletePostByGiveaway($giveaway);

    }


    public function listPostsByCodes(array $codes){

        $posts = $this->getDoctrine()->getRepository(Post::class)->listPostsByCodes($codes);

        return $posts;
    }

    public function listPostsByPath(array $paths){

        $posts = $this->getDoctrine()->getRepository(Post::class)->listPostsByPaths($paths);

        return $posts;
    }

    public function getPostEntityByCode(string $code){

        $post_entity = $this->getDoctrine()->getRepository(Post::class)->findOneBy(['code'=>$code]);

        return $post_entity;
    }

    public function getPostEntityByGiveaway(string $path){

        $post_entity = $this->getDoctrine()->getRepository(Post::class)->findOneBy(['giveaway_path'=>$path]);

        return $post_entity;
    }

    public function findPostsToParse(int $numbers){

        $post_entity = $this->getDoctrine()->getRepository(Post::class)->getPostsToParse($numbers);

        return $post_entity;
    }

    /**
     * @param $postData
     * @param $path
     * @param $limit
     * @param $count
     * @return bool
     * @throws \Exception
     */
    private function createPost($postData,$path,$limit,$count){

        try {
            $em = $this->getDoctrine()->getManager();

            $post = new Post();

            $post
                ->setStatus(0)
                ->setAfter(!empty($postData['after'])?$postData['after']:'')
                ->setLikes($postData['likes'])
                ->setCode($postData['code'])
                ->setCreated(time())
                ->setProfilePic($postData['profile_pic'])
                ->setCommentsLimit($limit)
                ->setChecked(0)
                ->setAccountId(0)
                ->setCommentsGet($count)
                ->setText($postData['caption'])
                ->setImageSrc($postData['image'])
                ->setLocation($postData['location'])
                ->setOwner($postData['username'])
                ->setGis($postData['rhx_gis'])
                ->setGiveawayPath($path)
                ->setCommentsPost($postData['comments']);

            $em->persist($post);
            $em->flush();
            $em->clear();

            return true;
        } catch (\Exception $exception){
            throw new \Exception($exception->getMessage(),400);
        }
    }

    public function updatePostStatusByGiveaway($path,$status){

        $em = $this->getDoctrine()->getManager();

        $post = $em->getRepository(Post::class)->findOneBy(['giveaway_path'=>$path]);

        $post
            ->setStatus($status);

        $em->persist($post);
        $em->flush();
        $em->clear();
    }


    public function updatePostEntityFromResponse($response) {

        $em = $this->getDoctrine()->getManager();

        foreach ($response['posts'] as $key=>$post) {

            $post_entity = $this->getPostEntityByGiveaway($post['giveaway']);

            $post_entity
                ->setCommentsGet($post_entity->getCommentsGet() + count($response['comments'][$post['giveaway']]))
                ->setStatus(empty($post['after'])?1:$post['status'])
                ->setChecked(time())
                ->setAfter((string)$post['after']);

            // If is "after" and status = false - we didn't complete parsing and stay it to next iteration
            if (!empty($post['after']) && !$post['status']) {

            }

            // If "after" is empty and status = false - we reached to the end of comments - stop parsing and begin find winner
            /*if (empty($post['after']) && !$post['status']) {
                $post_entity
                    ->setStatus(true);

                // todo: set event complete giveaway parsing -> goto find winner
            }

            // If "after" isn't empty and status = true - we reached to the limit of comments - stop parsing and begin find winner
            if (!empty($post['after']) && $post['status']) {

                // todo: set event complete giveaway parsing -> goto find winner
            }*/

            #$em->persist($post_entity);
            $em->flush();
            $em->clear();
        }

        return true;
    }

    /**
     * @param $code
     * @param $path
     * @param $limit
     * @return array
     * @throws \Exception
     */
    public function createPostByGiveaway($code,$path,$limit):array {

        $html = $this->parser->getHTMLPageContent([self::InstagramPostURL.$code]);

        try {

            $json = $this->json->getJsonFromHtml($html[0]['body']);

            $postData = $this->json->getPostDataFromJSON(json_decode($json));

            $comments = $postData['comments_list'];

            $this->createPost($postData,$path,$limit,count($comments));

            return $comments;

        } catch (\Exception $exception) {
            throw new \Exception('['.__FUNCTION__.'] '.$exception->getMessage());
        }
    }
}