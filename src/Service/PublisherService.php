<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 17.02.2019
 * Time: 9:30
 */

namespace App\Service;

use App\Controller\MainController;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;

class PublisherService extends MainController
{
    private $rabbit;

    public function __construct(ProducerInterface $producer){

        $this->rabbit = $producer;

    }

    public function sendRequestToStartGiveaway($giveaway){

        $message = [
            "giveaway"=>$giveaway,
            "task"=>"scrape-comments",
            "date"=>date('c'),
            "error"=> null
        ];

        $rabbitMessage = json_encode($message);

        $this->rabbit->publish($rabbitMessage,'application');

        return $message;
    }
}