<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 07.01.2019
 * Time: 17:13
 */

namespace App\Service;

use App\Entity\Voucher;
use App\Controller\MainController;

class VoucherService extends MainController
{

    public function findVoucherById(int $id){

        $voucher = $this->getDoctrine()->getRepository(Voucher::class)->find($id);

        return $voucher;
    }

    public function findVoucherByUniqueCode(string $code){

        $voucher = $this->getDoctrine()->getRepository(Voucher::class)->findOneBy(['code'=>$code]);

        return $voucher;
    }

    public function updateVoucherStatus($id,$status){

        $em = $this->getDoctrine()->getManager();

        $voucher = $this->findVoucherById($id);

        $voucher->setStatus($status);

        $em->persist($voucher);

        $em->flush();

        $em->clear();
    }
}