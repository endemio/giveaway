<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 06.02.2019
 * Time: 3:31
 */

namespace App\Service;


use App\Controller\MainController;
use App\Entity\Winner;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Psr\Log\LoggerInterface;

class WinnerService extends MainController {

    private $comments;

    private $rabbit;

    private $giveaway;

    private $logger;

    public function __construct(
        ProducerInterface $rabbit,
        LoggerInterface $logger,
        GiveawayService $giveawayService,
        CommentService $commentService
    ) {
        $this->comments = $commentService;

        $this->giveaway = $giveawayService;

        $this->rabbit = $rabbit;

        $this->logger = $logger;

    }

    public function findAllComments($giveaway){

        $comments = $this->comments->getAllCommentsByGiveaway($giveaway);

        return $comments;

    }

    private function getRandomBetween($start,$end){

        return rand($start,$end);

    }

    private function listWinnerByGiveaway($giveaway){

        $list = $this->getDoctrine()->getRepository(Winner::class)->getListByGiveaway($giveaway);

        return array_column($list,'account');

    }

    public function deleteWinnerByGiveaway($giveaway){

        $this->getDoctrine()->getRepository(Winner::class)->deleteEntityByGiveaway($giveaway);

    }

    public function checkCompleteChooseWinner($giveaway,$account,$options){

        $this->logger->info('['.__FUNCTION__.'] Start');

        $em = $this->getDoctrine()->getManager();

        $winner = $em->getRepository(Winner::class)->findOneBy(['giveaway' => $giveaway, 'account'=>$account]);

        $complete = true;

        if (!empty($options['liked']) && $options['liked'] && !$winner->getLiked()){

            $this->logger->info('['.__FUNCTION__.'] Liked false ');

            $complete = false;
        }

        if (!empty($options['follow']) && $options['follow'] && !$winner->getFollow()){

            $this->logger->info('['.__FUNCTION__.'] Follow false ');

            $complete = false;
        }

        if (!empty($options['repost']) && $options['repost'] && !$winner->getRepost()){

            $this->logger->info('['.__FUNCTION__.'] Repost false ');

            $complete = false;
        }

        /*if (!$winner->getComplete()){

            $complete = false;

        }*/

        $this->logger->info('['.__FUNCTION__.'] Complete = '.$complete);

        return $complete;

        /*if ($complete){
            // Set Giveaway as complete
            //$result['giveaway'] = true;
            $this->logger->info('['.__FUNCTION__.'] Start send email');

            $event = new GiveawayChooseWinnerCompleteEvent();
            $event->setGiveaway($giveaway);
            $event->setWinner(['account'=>$winner->getAccount(),'text'=>$winner->getText()]);
            $this->dispatcher->dispatch(GiveawayChooseWinnerCompleteEvent::NAME,$event);

            return true;
        }*/

    }

    /**
     * @param $path
     * @param $options
     * @return string
     */
    public function chooseWinner($path,$options):string {

        $this->logger->info('['.__FUNCTION__.'] Start');

        $dist = $this->findAllComments($path);

        $this->logger->info('['.__FUNCTION__.'] Comments = '.count($dist));

        // Get list account which have already been checked
        $winner_entity = $this->listWinnerByGiveaway($path);

        $this->logger->info('['.__FUNCTION__.'] Winners checked = '.count($winner_entity));

        // Get comments exclude winners have already been checked
        $comments = $this->returnCommentsListByOptions($dist,$options,$winner_entity);

        if (count($comments) == 0) {

            $this->logger->info('['.__FUNCTION__.'] There is no comments to choose from ');

            $this->giveaway->sendNotificationGiveawayError($path,$options,'['.__FUNCTION__.'] There is no comments to choose from ');

            $this->giveaway->updateGiveawayStatusByUnique($path,3);

            return '';
        }

        $this->logger->info('['.__FUNCTION__.'] Comments by options= '.count($comments));

        $winner = $this->getRandomBetween(0,count($comments)-1);

        $this->logger->info('['.__FUNCTION__.'] Winner comment = '.json_encode($comments[$winner]));

        $this->createWinner($path,$comments[$winner]);

        if (!empty($options['follow'])) {

            $this->logger->info('['.__FUNCTION__.'] Check for following target accounts');

            $this->sendRabbitToGetWinnerFollowingAccounts($comments[$winner]['owner'], $path);

            return '';
        }

        $complete = $this->checkCompleteChooseWinner($path,$comments[$winner]['owner'],$options);

        if ($complete){

            $this->giveaway->updateGiveawayStatusByUnique($path,3);

            $this->giveaway->sendNotificationGiveawayComplete($path,$comments[$winner]['owner']);

            try {


                $this->updateWinner($path, $comments[$winner]['owner'], [['name' => 'complete', 'value' => 1]]);
            } catch (\Exception $exception){
                $this->logger->error('['.__FUNCTION__.'] '.$exception->getMessage());
            }

            return '';
        }

        return $comments[$winner]['owner'];
    }

    /**
     * @param $account
     * @param $follow
     */
    private function sendRabbitToGetWinnerFollowingAccounts($account,$follow){

        // Check if list of following exists
        $filename = self::PythonScriptResultFolder . 'accounts/' . $account . '-following.data';

        if (is_file($filename)) {
            // Send request to Rabbit to get only message to result queue
            $message = [
                "task" => 'getlist',
                "timestamp" => date('c'),
                "account" => $account,
                "token" => $follow,
                "start" => 0,
                "limit" => 0,
                "type" => 'following_exists'];

        } else {
            // Send request to Rabbit to get list of following
            $message = [
                "task" => 'getlist',
                "timestamp" => date('c'),
                "account" => $account,
                "token" => $follow,
                "start" => 0,
                "limit" => 0,
                "type" => 'following'];
        }

        $rabbitMessage = json_encode($message);

        $this->rabbit->publish($rabbitMessage);

    }

    /**
     * @param $dist
     * @param $options
     * @param $winners
     * @return array
     */
    private function returnCommentsListByOptions($dist,$options,$winners){

        $comments = [];

        // Check if user want to find with text
        if (!empty($options['phrase']) && $options['phrase'] != null){

            foreach ($dist as $comment){

                if (
                    preg_match('/'.strtolower($options['phrase']).'/',strtolower($comment['text'])) &&
                    !in_array($comment['owner'],$winners)
                ){
                    array_push($comments,$comment);
                }
            }
        } else {
            $comments = $dist;
        }

        $result = [];

        // Check if user want to remove duplicates
        if (!empty($options['duplicates']) && $options['duplicates']){

            $temp = array_column($comments,'owner');

            $owners = array_unique($temp);

            foreach ($comments as $key=>$comment){
                if (in_array($comment['owner'],$owners) && !in_array($comment['owner'],$winners)){
                    array_push($result,$comment);
                    unset($owners[array_search($comment['owner'],$owners)]);
                }
            }

        } else {
            $result = $comments;
        }

        return $result;
    }


    private function createWinner($giveaway,$comment){

        $em = $this->getDoctrine()->getManager();

        $winner = new Winner();

        $winner
            ->setGiveaway($giveaway)
            ->setAccount($comment['owner'])
            ->setProfilePic($comment['profile_pic'])
            ->setCreated($comment['created'])
            ->setText($comment['text'])
            ->setFollow(false)
            ->setLiked(false)
            ->setRepost(false);

        $em->persist($winner);
        $em->flush();
        $em->clear();

    }

    public function getWinnerByGiveaway($giveaway){

        $winner = $this->getDoctrine()->getRepository(Winner::class)->findOneBy(['giveaway' => $giveaway,'complete'=>1]);

        return $winner;

    }

    /**
     * @param $giveaway
     * @param $account
     * @param $data
     * @throws \Exception
     */
    public function updateWinner($giveaway, $account, $data){

        $em = $this->getDoctrine()->getManager();

        $winner = $em->getRepository(Winner::class)->findOneBy(['giveaway' => $giveaway, 'account'=>$account]);

        $updated = false;
        foreach ($data as $el) {
            switch ($el['name']) {
                case 'liked':
                    $updated = true;
                    $winner->setLiked($el['value']?true:false);
                    break;
                case 'follow':
                    $updated = true;
                    $winner->setFollow($el['value']?true:false);
                    break;
                case 'repost':
                    $updated = true;
                    $winner->setRepost($el['value']?true:false);
                    break;
                case 'complete':
                    $updated = true;
                    $winner->setComplete($el['value']?true:false);
                    break;
            }
        }

        if ($updated) {
            try {
                $em->persist($winner);
                $em->flush();
                $em->clear();
            } catch (\Exception $exception) {
                throw new \Exception('[' . __FUNCTION__ . '] ' . $exception->getMessage(), 400);
            }
        }
    }

}