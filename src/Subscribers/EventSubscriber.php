<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 22.12.2018
 * Time: 5:07
 */

namespace App\Subscribers;

use App\Controller\MainController;
use App\Events\CommentsScrapingCompleteEvent;
use App\Events\CommentsScrapingStartEvent;
use App\Events\GiveawayChooseWinnerCompleteEvent;
use App\Events\GiveawayError;
use App\Events\GiveawayChooseWinnerStartEvent;
use App\Events\GiveawayStartEvent;
use App\Service\EmailService;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EventSubscriber extends MainController implements EventSubscriberInterface {

    private $email;

    private $logger;

    public function __construct(
        LoggerInterface $logger,
        EmailService $emailService
    ){
        $this->email = $emailService;
        $this->logger = $logger;
    }

    public static function getSubscribedEvents(){
        return array(
            GiveawayStartEvent::NAME                => 'onGiveawayStart',
            CommentsScrapingStartEvent::NAME        => 'onScrapingCommentsStart',
            CommentsScrapingCompleteEvent::NAME     => 'onScrapingCommentsComplete',
            GiveawayChooseWinnerCompleteEvent::NAME => 'onChooseWinnerComplete',
            GiveawayChooseWinnerStartEvent::NAME    => 'onChooseWinnerStart',
            GiveawayError::NAME                     => 'onChooseWinnerError'
        );
    }

    /**
     * @param GiveawayStartEvent $event
     * @throws \Exception
     */
    public function onGiveawayStart(GiveawayStartEvent $event){

        $this->logger->info('['.__FUNCTION__.'] Event Subscriber');

        try {
            $this->email->sendContactMail(
                'emailGiveawayStart.html.twig',
                'error@endemic.ru',
                ['pickergiveaway@gmail.com' , 'mail@endemic.ru'],
                //['pickergiveaway@gmail.com' , 'mail@endemic.ru'],
                'Giveaway Start ',
                [
                    'giveaway'=>$event->getPath(),
                    'options'=>json_encode($event->getOptions()),
                ]);

        } catch (\Exception $exception){
            $this->logger->error('['.__FUNCTION__.'] Get error '.$exception->getMessage());
            throw new \Exception('Email sending exception '.$exception->getMessage());
        }
    }

    /**
     * @param CommentsScrapingCompleteEvent $event
     * @throws \Exception
     */
    public function onScrapingCommentsStart(CommentsScrapingCompleteEvent $event){

        $this->logger->info('['.__FUNCTION__.'] Event Subscriber');

        try {
            $this->email->sendContactMail(
                'emailCommentsScrapingStart.html.twig',
                'error@endemic.ru',
                ['pickergiveaway@gmail.com' , 'mail@endemic.ru'],
                //['pickergiveaway@gmail.com' , 'mail@endemic.ru'],
                'Scraping Comments Start ',
                [
                    'giveaway'=>$event->getGiveaway()
                ]);

        } catch (\Exception $exception){
            $this->logger->error('['.__FUNCTION__.'] Get error '.$exception->getMessage());
            throw new \Exception('Email sending exception '.$exception->getMessage());
        }

    }

    /**
     * @param CommentsScrapingCompleteEvent $event
     * @throws \Exception
     */
    public function onScrapingCommentsComplete(CommentsScrapingCompleteEvent $event){

        $this->logger->info('['.__FUNCTION__.'] Event Subscriber');

        try {
            $this->email->sendContactMail(
                'emailCommentsScrapingComplete.html.twig',
                'error@endemic.ru',
                ['pickergiveaway@gmail.com' , 'mail@endemic.ru'],
                //['pickergiveaway@gmail.com' , 'mail@endemic.ru'],
                'Scraping Comments Complete ',
                [
                    'giveaway'=>$event->getGiveaway(),
                    'counter'=>$event->getCounter()
                ]);

        } catch (\Exception $exception){
            $this->logger->error('['.__FUNCTION__.'] Get error '.$exception->getMessage());
            throw new \Exception('Email sending exception '.$exception->getMessage());
        }

    }


    /**
     * @param GiveawayChooseWinnerStartEvent $event
     * @throws \Exception
     */
    public function onChooseWinnerStart(GiveawayChooseWinnerStartEvent $event){

        $this->logger->info('['.__FUNCTION__.'] Event Subscriber');

        try {
            $this->email->sendContactMail(
                'emailGiveawayStartChooseWinner.html.twig',
                'error@endemic.ru',
                ['pickergiveaway@gmail.com' , 'mail@endemic.ru'],
                //['pickergiveaway@gmail.com' , 'mail@endemic.ru'],
                'Giveaway Start Choose winner ',
                [
                    'giveaway'=>$event->getGiveaway(),
                    'options'=>json_encode($event->getOptions()),
                ]);

        } catch (\Exception $exception){
            $this->logger->error('['.__FUNCTION__.'] Get error '.$exception->getMessage());
            throw new \Exception('Email sending exception '.$exception->getMessage());
        }

    }

    /**
     * @param GiveawayChooseWinnerCompleteEvent $event
     * @throws \Exception
     */
    public function onChooseWinnerComplete(GiveawayChooseWinnerCompleteEvent $event){

        $this->logger->info('['.__FUNCTION__.'] Event Subscriber');

        try {
            $this->email->sendContactMail(
                'emailGiveawayCompleteChooseWinner.html.twig',
                'error@endemic.ru',
                ['pickergiveaway@gmail.com' , 'mail@endemic.ru'],
                //['pickergiveaway@gmail.com' , 'mail@endemic.ru'],
                'Giveaway Complete Choose winner ',
                [
                    'giveaway'=>$event->getGiveaway(),
                    'winner'=>json_encode($event->getWinner())
                ]);

        } catch (\Exception $exception){
            $this->logger->error('['.__FUNCTION__.'] Get error '.$exception->getMessage());
            throw new \Exception('Email sending exception '.$exception->getMessage());
        }

    }

    /**
     * @param GiveawayError $event
     * @throws \Exception
     */
    public function onChooseWinnerError(GiveawayError $event){

        $this->logger->info('['.__FUNCTION__.'] Event Subscriber');

        try {
            $this->email->sendContactMail(
                'emailGiveawayChooseWinnerError.html.twig',
                'error@endemic.ru',
                ['pickergiveaway@gmail.com' , 'mail@endemic.ru'],
                //['pickergiveaway@gmail.com' , 'mail@endemic.ru'],
                'Giveaway Complete Choose winner ',
                [
                    'giveaway'=>$event->getGiveaway(),
                    'options'=>json_encode($event->getOptions()),
                    'message'=>$event->getMessage()
                ]);

        } catch (\Exception $exception){
            $this->logger->error('['.__FUNCTION__.'] Get error '.$exception->getMessage());
        }

    }


}

#['pickergiveaway@gmail.com' , 'mail@endemic.ru'],